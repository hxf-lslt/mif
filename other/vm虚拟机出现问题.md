# 虚拟机出现的问题总结和解决方案



## 1.使用ifconfig命令查询IP地址为空

![image-20240422155951016](vm虚拟机出现问题.assets/image-20240422155951016.png)

同时无法ping通网关。

解决方案：

 1. 停止NetworkManager

    ```sh
    systemctl stop NetworkManager
    ```

 2. 清空NetworkManager

    ```sh
    systemctl disable NetworkManager
    ```

 3. 重启网络服务

    ```sh
    service network restart
    ```

 4. 再次查看IP地址

    ```sh
    ifconfig
    ```

    没有问题，同时也可以ping通网关和外部网站。

