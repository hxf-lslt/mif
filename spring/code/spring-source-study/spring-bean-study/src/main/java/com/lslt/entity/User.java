package com.lslt.entity;

import org.springframework.beans.factory.InitializingBean;

/**
 * @author lslt
 * @description
 * @date 2024/5/18 15:45
 */
public class User implements InitializingBean {

    private String username;

    private String age;

    public User() {
        System.out.println("实例化user");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", age=" + age +
                '}';
    }

    public void init(){
        System.out.println("执行初始化方法");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("实现 InitializingBean 接口实现初始化后调用");
    }
}
