package com.lslt.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author lslt
 * @description
 * @date 2024/5/18 23:28
 */
//@Configuration
public class MyBeanPostProcessor implements BeanPostProcessor {

    Properties properties;
    Pattern compile;


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("初始化之前"+bean);
        setObjectFieldValue(bean);
        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("初始化之后"+bean);
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }

    private void setObjectFieldValue(Object bean){
        Class<?> beanClass = bean.getClass();
        Field[] declaredFields = beanClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            try {
                String key = declaredField.get(bean).toString();
                Matcher matcher = compile.matcher(key);
                if(matcher.find()){
                    key = matcher.group(1);
                }
                Object value = properties.get(key);
                if(!StringUtils.isEmpty(value)){
                    declaredField.set(bean,value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void init(){
        compile = Pattern.compile("\\$\\{([^}]*)\\}");
        try {
            properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("bean.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
