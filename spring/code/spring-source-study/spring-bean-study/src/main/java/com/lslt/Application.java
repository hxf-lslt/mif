package com.lslt;

import com.lslt.entity.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author lslt
 * @description
 * @date 2024/5/18 15:37
 */
public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("service.xml");
        User user = context.getBean("user", User.class);
        System.out.println(user);
    }
}
