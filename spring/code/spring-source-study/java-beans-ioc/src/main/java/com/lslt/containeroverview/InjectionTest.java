package com.lslt.containeroverview;

import com.lslt.repository.UserRepository;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author lslt
 * @description 依赖注入
 * @date 2024/5/23 22:32
 */
public class InjectionTest {

    /**
     * 结论：
     * 1.依赖查找的bean和依赖注入的bean不是同一个东西
     */
    public static void main(String[] args) {
        BeanFactory beanFactory = new ClassPathXmlApplicationContext("classpath:/bean-container-injection.xml");

        UserRepository userRepository = beanFactory.getBean("userRepository", UserRepository.class);
//        System.out.println(userRepository);

        System.out.println(userRepository.getBeanFactory());//org.springframework.beans.factory.support.DefaultListableBeanFactory@78e94dcf
        System.out.println(beanFactory);//org.springframework.context.support.ClassPathXmlApplicationContext@35bbe5e8
        System.out.println(userRepository.getBeanFactory() == beanFactory);

        System.out.println(userRepository.getBeanFactory().getBean("user"));//User{id=1, name='张三'}

//        System.out.println(beanFactory.getBean(BeanFactory.class));//No qualifying bean of type 'org.springframework.beans.factory.BeanFactory' available


        //ObjectFactory 中的泛型是User
//        System.out.println(userRepository.getObjectFactory().getObject());//SuperUser{address='上海'} User{id=1, name='张三'} 因为superUser是primary所以拿到的是superUser

        //ObjectFactory 中的泛型是ApplicationContext
        System.out.println(userRepository.getObjectFactory().getObject());//org.springframework.context.support.ClassPathXmlApplicationContext@35bbe5e8 与当前的BeanFactory是同一个对象
    }
}
