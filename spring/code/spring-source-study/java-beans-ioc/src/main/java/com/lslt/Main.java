package com.lslt;

import com.lslt.pojo.Person;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyEditorSupport;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.stream.Stream;

/**
 * @author lslt
 * @description
 * @date 2024/5/21 23:58
 */
public class Main {
    public static void main(String[] args) throws IntrospectionException {

        Person person = new Person();

        BeanInfo beanInfo = Introspector.getBeanInfo(Person.class, Object.class);

        Stream.of(beanInfo.getPropertyDescriptors()).forEach(propertyDescriptor -> {
            Class<?> propertyType = propertyDescriptor.getPropertyType();
            System.out.println(propertyType);
            String propertyName = propertyDescriptor.getName();

            if("age".equals(propertyName)){
                propertyDescriptor.setPropertyEditorClass(StringToIntegerPropertyEditor.class);
                propertyDescriptor.createPropertyEditor(person);
            }
            System.out.println(propertyDescriptor);
        });
        try {
            Method setAge = person.getClass().getMethod("setAge", Integer.class);
            setAge.invoke(person,12);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        System.out.println(person);
    }

    static class StringToIntegerPropertyEditor extends PropertyEditorSupport {

        public void setAsText(String text) throws java.lang.IllegalArgumentException {
           Integer value = Integer.valueOf(text);
           setValue(value);
        }
    }
}