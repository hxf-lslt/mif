


### BeanNameGenerator(Bean 的名字生成器)

 > 1. DefaultBeanNameGenerator 默认的名字生成器
 > 2. AnnotationBeanNameGenerator 注解模式下的Bean命名，@Component

### BeanDefinition的注册

> 1. 通过xml的方式
> 2. 通过注解的模式（配置类）
> 3. 通过Java Api 的模式



### 实例化bean

> 1. 通过 xml 中配制 factory-method 来指定实例化 bean 的方法。其中方法是静态的
> 2. 通过对象工厂去实例化 Bean xml 需要配置 factory-bean 和 factory-method 分别来指代创建 bean 的工厂和方法
> 3. 通过 FactoryBean 去实例化 Bean
> 4. 通过 ServiceLoader 去加载并实例化 Bean （java.util.ServiceLoader）。
>  Spring 中的 ServiceLoaderFactoryBean就是使用这种机制
> 5. 通过 AutowireCapableBeanFactory 去创建Bean
>

### Bean 的初始化方式：

> 1.  @PostStruct 注解
> 2. 实现 InitializingBean#afterPropertiesSet() 方法
> 3. 在 xml 或 @Bean 添加 initMethod 属性
> 
调用顺序 @PostStruct -> InitializingBean -> initMethod

### Bean 的销毁方式

> 1. @PreDestroy 注解
> 2. 实现 DisposableBean#destroy() 方法
> 3. @Bean 添加 destroyMethod 属性
>
调用顺序 @PreDestroy -> DisposableBean -> destroyMethod

### Bean 的处理器

> ConfigurationClassPostProcessor 相当于与配置类。处理 @Configuration 等的 BeanFactoryPostProcessor 后置处理器
> AutowiredAnnotationBeanPostProcessor bean 的后置处理器(BeanPostProcessor)，处理 @Autowired @Value 注解
> CommonAnnotationBeanPostProcessor bean 的后置处理器(BeanPostProcessor)，处理 @PostConstruct @PreDestroy 注解
> EventListenerMethodProcessor  BeanFactoryPostProcessor 处理器，处理 @EventListener
> 
> Bean 的自动配置
Autowire:
> NO: 不开启
> BY_NAME: 通过名字
> BY_TYPE: 通过类型
> CONSTRUCTOR: 构造器注入 通过构造器是一种特殊的通过类型注入的方式，可以看 AutowireCapableBeanFactory 中定义的类型

### 依赖注入

> 1. setter 注入
>  手动模式：
>     a. xml 方式注入 需要手动的设置 property 属性
>     b. 注解的模式
>     c. Api 注入模式
>    自动模式：
>     a. byType xml添加属性 autowire = "byType"
>     b. byName xml添加属性 autowire = "byName"
> 2. 构造器注入
>  手动模式：
>     a. xml 配置 constructor-arg 属性
>     b. 注解模式
>     c. Api 模式
>     自动模式
>    a.在 xml 配置 autowire = "constructor"
> 3. 字段注入
>    a. @Autowired 不会处理 static 字段
>    b. @Resource
>    c. @Inject
> 4. 方法注入
>    a. @Autowired 不会处理 static 字段
>    b. @Resource
>    c. @Inject
>    d. @Bean
> 5. 接口方法回调
>   Aware 的一系列接口，如：BeanFactoryAware ApplicationContextAware 等
> 

### 依赖处理过程

入口：DefaultListableBeanFactory#resolveDependency
依赖描述符：DependencyDescriptor
自定绑定候选对象处理器：AutowireCandidateResolver

DefaultListableBeanFactory.resolveDependency源码解析

```java
	public Object resolveDependency(DependencyDescriptor descriptor, @Nullable String requestingBeanName,
			@Nullable Set<String> autowiredBeanNames, @Nullable TypeConverter typeConverter) throws BeansException {

		descriptor.initParameterNameDiscovery(getParameterNameDiscoverer());
        //首先会判断需要注入的对象是不是Optional类型的
		if (Optional.class == descriptor.getDependencyType()) {
			return createOptionalDependency(descriptor, requestingBeanName);
		}
        //判断需要注入对象是不是ObjectFactory或ObjectProvider类型
		else if (ObjectFactory.class == descriptor.getDependencyType() ||
				ObjectProvider.class == descriptor.getDependencyType()) {
			return new DependencyObjectProvider(descriptor, requestingBeanName);
		}
        //JSR330
		else if (javaxInjectProviderClass == descriptor.getDependencyType()) {
			return new Jsr330Factory().createDependencyProvider(descriptor, requestingBeanName);
		}
		else {
            //会处理延迟加载
			Object result = getAutowireCandidateResolver().getLazyResolutionProxyIfNecessary(
					descriptor, requestingBeanName);
			if (result == null) {
                //上面的条件不符合时，就会去解析
				result = doResolveDependency(descriptor, requestingBeanName, autowiredBeanNames, typeConverter);
			}
			return result;
		}
	}
```

DefaultListableBeanFactory.doResolveDependency源码解析

```java
public Object doResolveDependency(DependencyDescriptor descriptor, @Nullable String beanName,
			@Nullable Set<String> autowiredBeanNames, @Nullable TypeConverter typeConverter) throws BeansException {

    //这个是去坐上下级依赖时用到
		InjectionPoint previousInjectionPoint = ConstructorResolver.setCurrentInjectionPoint(descriptor);
		try {
            //快照
			Object shortcut = descriptor.resolveShortcut(this);
			if (shortcut != null) {
				return shortcut;
			}

			Class<?> type = descriptor.getDependencyType();
			...
			//找到符合类型的 bean 的名称的集合
			Map<String, Object> matchingBeans = findAutowireCandidates(beanName, type, descriptor);
			...
			// 如果找到多个
			if (matchingBeans.size() > 1) {
                //会去判断是不是有 primary=true 属性的 BeanDefinition
				autowiredBeanName = determineAutowireCandidate(matchingBeans, descriptor);
				if (autowiredBeanName == null) {
					if (isRequired(descriptor) || !indicatesMultipleBeans(type)) {
						return descriptor.resolveNotUnique(descriptor.getResolvableType(), matchingBeans);
					}
				}
                // 找到需要注入的 bean 的 CLass  对象
				instanceCandidate = matchingBeans.get(autowiredBeanName);
			}
			else {
				// We have exactly one match.
				Map.Entry<String, Object> entry = matchingBeans.entrySet().iterator().next();
				autowiredBeanName = entry.getKey();
				instanceCandidate = entry.getValue();
			}

			if (autowiredBeanNames != null) {
				autowiredBeanNames.add(autowiredBeanName);
			}
			if (instanceCandidate instanceof Class) {
                //通过 BeanFactory.getBean 获取到 bean 实例
				instanceCandidate = descriptor.resolveCandidate(autowiredBeanName, type, this);
			}
			Object result = instanceCandidate;
            ...
            //将结果返回
			return result;
		}
		finally {
			ConstructorResolver.setCurrentInjectionPoint(previousInjectionPoint);
		}
	}
```

如果是集合类型的 Field 比如 Map、Array... 就会走 resolveMultipleBeans 的方法。

### @Autowired 注入

元信息解析

依赖查找

依赖注入（字段，方法）

  ![Bean实例化-Autowire注解流程](readme.assets/Bean实例化-Autowire注解流程.svg)

### Java通用注解注入原理

> CommonAnnotationBeanPostProcessor：
>
> ​	注入注解：
>
> 1. javax.xml.ws.WebServiceRef
>
> 2. Resource
>
> 3. javax.ejb.EJB
>
> 
>
>  ​ 生命周期注解：
>
> 1. PostConstruct
> 2. PreDestroy

在处理BeanPostProcessor时会根据Order的值来确定谁先执行，其中Order中的值越小优先级越高



### Bean依赖来源

> 1. BeanDefinition作为依赖来源
>
>    元数据：BeanDefinition
>
>    注册：BeanDefinitionRegister#registerBeanDefinition
>
>    类型：延迟和非延迟
>
>    顺序：Bean 生命周期顺序按照注册顺序

### Bean作用域

> singleton: 单例模式，每次获取都是同一个对象。
>
> prototype: 原型模式，每次获取都是不同的对象。
>
> request: （RequestScope）在 MVC 架构中，每次请求都是不一样的对象。
>
> session: （SessionScope）在 MVC 中，在一个 session 中，请求的对象是一样的，里面会有同步锁，有一定的性能损耗。
>
> application：(ServletContexctScope)

### Spring Bean 生命周期

> 1. Spring Bean 元信息配置
>
>    面向资源
>
>    - xml 配置
>    - Properties 资源配置
>    
>    Bean
>    
>    面向注解
>    
>    面向 API

#### Spring Bean 元信息配置

1. 面向资源

- xml 配置
- Properties 资源配置

​	BeanDefinition 解析

 			BeanDefinitionReader

​			xml解析器：BeanDefinitionParser

2. 面向注解

   AnnotatedBeanDefinitionReader

3. 面向 API

### Spring Bean 注册阶段

BeanDefinition 注册接口：

- BeanDefinitionRegistry： 主要实现类 DefaultListableBeanFactory

  注册方法：registerBeanDefinition()

  还有一个BeanDefinitionReaderUtils中有个注册BeanDefinition方法registerBeanDefinition其实就是在调用DefaultListableBeanFactory#registerBeanDefinition

使用 xml 解析时： DefaultBeanDefinitionDocumentReader#processBeanDefinition()里面注册时就是调用BeanDefinitionReaderUtils的注册方法。

使用注解时也是一样的在AnnotatedBeanDefinitionReader#doRegisterBean也是调用BeanDefinitionReaderUtils的注册方法。

### Spring BeanDefinition 合并阶段

> BeanDefinition 合并：
>
> - 父子 BeanDefinition 合并
>   - 当前 BeanFactory 查找
>   - 层次 BeanFactory 查找
>
> 比如之类继承了父类的属性，子类中属性的值将会去父类中查找

![mergerBeanDefinition流程](readme.assets/mergerBeanDefinition流程.svg)
