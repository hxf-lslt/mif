package com.lslt.container;

import com.lslt.annotation.InjectedUser;
import com.lslt.annotation.MyAutowired;
import com.lslt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.inject.Inject;
import java.util.Map;

/**
 * @author lslt
 * @description 测试注解类型的以来注入过程
 * @date 2024/6/2 17:09
 */
@Configurable
public class AnnotationDependencyInjectionResolverTest {


    @Autowired
    private User user;  //DependencyDescriptor ->
    //require = true 必须的
    //eager = true 实时
    // 通过类型 (User.class) 来进行依赖查找，
    // 字段名称 ('user')

    //    @Autowired
    private Map<String, User> users;


    @Inject
    private User injectUser;

    @MyAutowired
    private User myAutowiredUser; // 使用自定义注解看看是否可以注入


    @InjectedUser
    private User injectedUser;

//    @Bean(name = "injectedAnnotationBeanPostProcessor")
//    @Bean(name = AUTOWIRED_ANNOTATION_PROCESSOR_BEAN_NAME)
//    public static AutowiredAnnotationBeanPostProcessor beanPostProcessor(){
//        AutowiredAnnotationBeanPostProcessor beanPostProcessor = new AutowiredAnnotationBeanPostProcessor();
//
//        Set<Class<? extends Annotation>> autowiredAnnotationTypes = new LinkedHashSet<>(Arrays.asList(Autowired.class,Inject.class, InjectedUser.class));
////        beanPostProcessor.setAutowiredAnnotationType(InjectedUser.class);
//        beanPostProcessor.setAutowiredAnnotationTypes(autowiredAnnotationTypes);
//        return beanPostProcessor;
//    }

    /**
     * 添加了 static 会优先处理，不加 static 就是需要等 AnnotationDependencyInjectionResolverTest 在实例化的时候才会去处理，等到那个时候 beanPostProcessor 就失去了效果
     */
    @Bean(name = "injectedAnnotationBeanPostProcessor")
    @Order(Ordered.LOWEST_PRECEDENCE - 3)
    public static AutowiredAnnotationBeanPostProcessor beanPostProcessor() {
        AutowiredAnnotationBeanPostProcessor beanPostProcessor = new AutowiredAnnotationBeanPostProcessor();
        beanPostProcessor.setAutowiredAnnotationType(InjectedUser.class);
        return beanPostProcessor;
    }


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(AnnotationDependencyInjectionResolverTest.class);

        //xmlBeanDefinition 解析器 读取配置文件
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(applicationContext);
        //加载 BeanDefinition
        String resourcePath = "classpath:/dependence-injection.xml";
        xmlBeanDefinitionReader.loadBeanDefinitions(resourcePath);

        //启动容器
        applicationContext.refresh();


        //获取 bean
        AnnotationDependencyInjectionResolverTest bean = applicationContext.getBean(AnnotationDependencyInjectionResolverTest.class);

        System.out.println(bean.user); //SuperUser{address='上海'} User{id=1, name='张三'}

        System.out.println(bean.users);

        System.out.println(bean.injectUser);

        System.out.println("myAutowired 注入：" + bean.myAutowiredUser); // myAutowired 注入：SuperUser{address='上海'} User{id=1, name='张三'}
        System.out.println("InjectedUser 注入：" + bean.injectedUser);
        applicationContext.close();
    }
}
