package com.lslt.repository;

import com.lslt.pojo.User;

/**
 * @author lslt
 * @description
 * @date 2024/5/28 20:39
 */
public class UserHolder {

    private User user;


    public UserHolder() {
    }

    public UserHolder(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserHolder{" +
                "user=" + user +
                '}';
    }
}
