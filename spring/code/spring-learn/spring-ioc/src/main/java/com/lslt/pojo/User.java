package com.lslt.pojo;

import org.springframework.beans.factory.BeanNameAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author lslt
 * @description
 * @date 2024/5/22 22:56
 */
public class User implements BeanNameAware {

    private Long id;
    private String name;

    private String beanName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }


    //方法必须是静态的，因为是要去实例化 bean 如果是非静态的那不是需要对象去调用吗，与本身要实例化对象冲突
    public static User createUser() {
//        System.out.println("==== 实例化 Bean ====");
        User user = new User();
        user.setId(2L);
        user.setName("张三");
        return user;
    }

    @PostConstruct
    public void init() {
        System.out.println("User Bean 名称[" + beanName + "]正在执行初始化...");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("User Bean 名称[" + beanName + "]正在销毁中...");
    }


    @Override
    public void setBeanName(String name) {
        this.beanName = name;
    }
}
