package com.lslt.factory;

import com.lslt.pojo.User;

/**
 * @author lslt
 * @description
 * @date 2024/5/26 14:00
 */
public interface UserFactory {

    default  User createUser(){
        return User.createUser();
    }
}
