package com.lslt.pojo;

/**
 * @author lslt
 * @description
 * @date 2024/6/4 23:27
 */
public class Vehicle {

    private String name;

    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
