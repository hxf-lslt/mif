package com.lslt.container;

import com.lslt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.util.Map;

/**
 * @author lslt
 * @description bean 作用域测试
 * @date 2024/6/4 12:40
 */
@Configurable
public class BeanScopeTest {


    @Bean
    public User singletonUser() {
        return createUser();
    }


    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public User prototypeUser() {
        return createUser();
    }

    private User createUser() {
        User user = new User();
        user.setId(System.currentTimeMillis());
        // 太快的话 prototype 类型 和 singleton 生成的 Bean 的ID 一致，可以查看 实例的 hashCode 值
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
        return user;
    }

    @Autowired
    @Qualifier("singletonUser")
    private User singletonUser;

    @Autowired
    @Qualifier("singletonUser")
    private User singletonUser1;

    @Autowired
    @Qualifier("prototypeUser")
    private User prototypeUser;

    @Autowired
    @Qualifier("prototypeUser")
    private User prototypeUser1;

    @Autowired
    @Qualifier("prototypeUser")
    private User prototypeUser2;

    @Autowired
    private Map<String, User> users;

    /**
     * 1. singleton Bean 只会创建一次, prototype Bean 每次依赖查找或依赖注入均会生成新的实例
     * <p>
     * 2. singleton Bean 和 prototype Bean 在注入集合字段时，只会注入一次，且与前面的注入的实例均不一致
     * <p>
     * 3. singleton Bean 和 prototype Bean 均会执行 init 方法回调 但是只有 singleton Bean 会执行 destroy 方法
     */

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(BeanScopeTest.class);

        applicationContext.refresh();

        scopeBeansByLookup(applicationContext);
        scopeBeansByDependency(applicationContext);
        applicationContext.close();
    }

    private static void scopeBeansByDependency(AnnotationConfigApplicationContext applicationContext) {
        //通过依赖注入
        BeanScopeTest bean = applicationContext.getBean(BeanScopeTest.class);

        System.out.println(bean.singletonUser + " hashCode: " + bean.singletonUser.hashCode());
        System.out.println(bean.singletonUser1 + " hashCode: " + bean.singletonUser1.hashCode());
        System.out.println(bean.prototypeUser + " hashCode: " + bean.prototypeUser.hashCode());
        System.out.println(bean.prototypeUser1 + " hashCode: " + bean.prototypeUser1.hashCode());
        System.out.println(bean.prototypeUser2 + " hashCode: " + bean.prototypeUser2.hashCode());
        System.out.println(bean.users);
    }

    private static void scopeBeansByLookup(AnnotationConfigApplicationContext applicationContext) {
        //通过依赖查找查询 Bean
        for (int i = 0; i < 3; i++) {
            //查找单例 Bean
            User singletonUser = applicationContext.getBean("singletonUser", User.class);
            System.out.println(singletonUser + " hashCode: " + singletonUser.hashCode());

            //查找原型 Bean
            User prototypeUser = applicationContext.getBean("prototypeUser", User.class);
            System.out.println(prototypeUser + " hashCode: " + prototypeUser.hashCode());
        }
    }
}
