package com.lslt.containeroverview;

import com.lslt.repository.UserRepository;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author lslt
 * @description 依赖注入
 * @date 2024/5/23 22:32
 */
public class InjectionTest {

    /**
     * 结论：
     * 1.依赖查找的bean和依赖注入的bean不是同一个东西
     */
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/bean-container-injection.xml");

        UserRepository userRepository = applicationContext.getBean("userRepository", UserRepository.class);
//        System.out.println(userRepository);

        System.out.println(userRepository.getBeanFactory());//org.springframework.beans.factory.support.DefaultListableBeanFactory@78e94dcf
        System.out.println(applicationContext);//org.springframework.context.support.ClassPathXmlApplicationContext@35bbe5e8
        System.out.println(userRepository.getBeanFactory() == applicationContext);

        System.out.println(userRepository.getBeanFactory().getBean("user"));//User{id=1, name='张三'}

//        System.out.println(applicationContext.getBean(BeanFactory.class));//No qualifying bean of type 'org.springframework.beans.factory.BeanFactory' available


        //ObjectFactory 中的泛型是User
//        System.out.println(userRepository.getObjectFactory().getObject());//SuperUser{address='上海'} User{id=1, name='张三'} 因为superUser是primary所以拿到的是superUser

        //ObjectFactory 中的泛型是ApplicationContext
        System.out.println(userRepository.getObjectFactory().getObject());//org.springframework.context.support.ClassPathXmlApplicationContext@35bbe5e8 与当前的BeanFactory是同一个对象
    }


    /**
     * 在底层的实现中 ApplicationContext 中组合了一个 BeanFactory。
     * 在 ApplicationContext 获取 Bean 其实就是调用 BeanFactory#getBean()
     * <p>
     *  AbstractRefreshableApplicationContext#getBeanFactory()
     *  虽然 ApplicantContext 实现了 BeanFactory 接口，但是获取到的 BeanFactory 对象和  ApplicationContext 对象不是同一个对象
     */
    private static void whoIsIocContainer(UserRepository userRepository,BeanFactory beanFactory){
        System.out.println(userRepository.getBeanFactory() == beanFactory); //false
    }
}
