package com.lslt.factory;

import com.lslt.pojo.User;
import org.springframework.beans.factory.FactoryBean;

/**
 * @author lslt
 * @description
 * @date 2024/5/26 14:10
 */
public class UserFactoryBean implements FactoryBean<User> {
    @Override
    public User getObject() throws Exception {
        return User.createUser();
    }

    @Override
    public Class<?> getObjectType() {
        return User.class;
    }
}
