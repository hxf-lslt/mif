package com.lslt.definition;

import com.lslt.pojo.User;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.*;

/**
 * @author lslt
 * @description {@link org.springframework.beans.factory.config.BeanDefinition} 构建
 * @date 2024/5/25 23:58
 */
public class BeanDefinitionTest {

    /**
     * 构建 BeanDefinition 有两种方式
     *  1.通过 BeanDefinitionBuilder 构建
     *  2.通过 AbstractBeanDefinition 及派生类实现
     */

    public static void main(String[] args) {

        /////////////////通过 BeanDefinitionBuilder////////////////////////

        //获取 BeanDefinitionBuilder
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(User.class);
        //设置属性
        builder.addPropertyValue("id",1);
        builder.addPropertyValue("name","张三");
        //获取 BeanDefinition
        BeanDefinition beanDefinition = builder.getBeanDefinition();


        /////////////////通过 AbstractBeanDefinition/////////////////////////

        //获取 BeanDefinition 对象
        GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
        //设置 BeanClass
        genericBeanDefinition.setBeanClass(User.class);
        //设置属性
        MutablePropertyValues propertyValues = new MutablePropertyValues();
        propertyValues.add("id",1);
        propertyValues.add("name","张三");
        genericBeanDefinition.setPropertyValues(propertyValues);

        //注册 BeanDefinition
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        registerBeanDefinition(beanFactory,"user", User.class);
//        System.out.println(beanFactory.getBeanDefinition("user"));
        //非命名的方式，spring 注册时会自动的生成名字
        System.out.println(beanFactory.getBeanDefinition("com.lslt.pojo.User#0"));

    }

    //BeanDefinition 注册
    public static void registerBeanDefinition(BeanDefinitionRegistry registry,String beanName,Class<?> beanClass){
        //构建 BeanDefinition
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(beanClass);
        builder.addPropertyValue("id",2L).addPropertyValue("name","王五");
        AbstractBeanDefinition beanDefinition = builder.getBeanDefinition();

        //注册 BeanDefinition (有名字的)
//        registry.registerBeanDefinition(beanName,beanDefinition);

        //非命名方式
        BeanDefinitionReaderUtils.registerWithGeneratedName(beanDefinition,registry);

    }

}
