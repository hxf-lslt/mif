package com.lslt.dependenceInjection;

import com.lslt.pojo.User;
import com.lslt.repository.UserHolder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author lslt
 * @description
 * @date 2024/5/28 21:45
 */
public class ApiSetterInjectionTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(AnnotationSetterInjectionTest.class);

        //注册 BeanDefinition
        applicationContext.registerBeanDefinition("userHolder",createUserHolderBeanDefinition());

        applicationContext.refresh();

        UserHolder userHolder = applicationContext.getBean(UserHolder.class);
        System.out.println(userHolder);

        applicationContext.close();
    }

      //setter 注入
//    private static BeanDefinition createUserHolderBeanDefinition(){
//        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(UserHolder.class);
//        //添加属性引用
//        builder.addPropertyReference("user","user");
//        return builder.getBeanDefinition();
//    }

    //构造器注入
    private static BeanDefinition createUserHolderBeanDefinition(){
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(UserHolder.class);
        //添加构造器引用
        builder.addConstructorArgReference("user");
        return builder.getBeanDefinition();
    }


    @Bean("user")
    public User createUser(){
        User user = new User();
        user.setId(2L);
        user.setName("李四");
        return user;
    }
}
