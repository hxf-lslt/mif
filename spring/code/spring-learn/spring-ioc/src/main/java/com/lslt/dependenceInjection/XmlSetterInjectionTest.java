package com.lslt.dependenceInjection;

import com.lslt.repository.UserHolder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * @author lslt
 * @description setter 注入
 * @date 2024/5/28 20:40
 */
public class XmlSetterInjectionTest {

    public static void main(String[] args) {
        //BeanFactory 实现了 BeanDefinitionRegister
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        //xmlBeanDefinition 解析器
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(beanFactory);

        //加载 BeanDefinition
        String resourcePath = "classpath:/dependence-injection.xml";
        xmlBeanDefinitionReader.loadBeanDefinitions(resourcePath);

        //依赖查找 Bean 同时会向容器中初始化 Bean
        UserHolder userHolder = beanFactory.getBean(UserHolder.class);
        System.out.println(userHolder);
    }
}
