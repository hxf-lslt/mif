package com.lslt.container;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.Resource;

/**
 * @author lslt
 * @description 使用外部文件进行配制
 * @date 2024/6/4 0:16
 */
@Configuration
@PropertySource(value = "classpath:/default.properties", encoding = "UTF-8")
public class ExternalConfigurationDependencySourceTest {


    @Value("${user.id:-1}")
    private Long id;

    @Value("${user.name}")
    private String name;

    @Value("${usr.name}")
    private String username;

    @Value("${user.resource:classpath:/no/default.properties}")
    private Resource resource;


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(ExternalConfigurationDependencySourceTest.class);

        applicationContext.refresh();

        ExternalConfigurationDependencySourceTest bean = applicationContext.getBean(ExternalConfigurationDependencySourceTest.class);

        System.out.println(bean.id); // 1
        System.out.println(bean.name); // hxfgs 获取的不是我在 properties 文件中配制的值 '张三' 而是拿到了我当前电脑的用户名字,由于系统变量的优先级更高
        System.out.println(bean.username); //  张三
        System.out.println(bean.resource); //class path resource [default.properties]
        applicationContext.close();
    }
}
