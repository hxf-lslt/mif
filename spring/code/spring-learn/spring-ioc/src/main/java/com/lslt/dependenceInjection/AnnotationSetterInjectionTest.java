package com.lslt.dependenceInjection;

import com.lslt.pojo.SuperUser;
import com.lslt.pojo.User;
import com.lslt.repository.UserHolder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author lslt
 * @description 注解方式的 setter 注入
 * @date 2024/5/28 20:54
 */
public class AnnotationSetterInjectionTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(AnnotationSetterInjectionTest.class);

        /*
         * 如果配合 xml 读取 User 的话需要在调用 refresh() 方法之前使用。
         * 需要将 xml 文件中 userHolder 注释掉
         * @Bean 注解好像没有 primary 属性。
            XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(applicationContext);

            //加载 BeanDefinition
            String resourcePath = "classpath:/dependence-injection.xml";
            xmlBeanDefinitionReader.loadBeanDefinitions(resourcePath);
        */

        applicationContext.refresh();

        UserHolder userHolder = applicationContext.getBean(UserHolder.class);

        System.out.println(userHolder);

        applicationContext.close();
    }

    ///////////////////////setter 注入//////////////////////////
//    @Bean
//    public UserHolder userHolder(User user){
//        UserHolder userHolder = new UserHolder();
//        userHolder.setUser(user);
//        return userHolder;
//    }
    ///////////////////////构造器注入//////////////////////////
    @Bean
    public UserHolder userHolder(User user){
        return new UserHolder(user);
    }

    @Bean("user")
    public User user(){
        User user = new User();
        user.setId(2L);
        user.setName("李四");
        return user;
    }

    @Bean(name = "superUser")
    public User superUser(){
        SuperUser superUser = new SuperUser();
        superUser.setAddress("北京");
        superUser.setId(3L);
        superUser.setName("王五");
        return superUser;
    }
}
