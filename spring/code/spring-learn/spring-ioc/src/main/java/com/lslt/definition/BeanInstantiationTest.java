package com.lslt.definition;

import com.lslt.factory.DefaultUserFactory;
import com.lslt.factory.UserFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author lslt
 * @description bean 的实例化
 * @date 2024/5/26 13:39
 */
public class BeanInstantiationTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:/bean-instantiation.xml");

        /*
        User userInstantiationByFactoryMethod = applicationContext.getBean("userInstantiationByFactoryMethod", User.class);
        User userInstantiationByFactoryBean = applicationContext.getBean("userInstantiationByFactoryBean", User.class);
        User userInstantiationByFactoryBean2 = applicationContext.getBean("userInstantiationByFactoryBean2", User.class);

        System.out.println(userInstantiationByFactoryMethod);
        System.out.println(userInstantiationByFactoryBean);
        System.out.println(userInstantiationByFactoryBean2);
        System.out.println(userInstantiationByFactoryMethod == userInstantiationByFactoryBean); //false 创造的是不同的对象
        System.out.println(userInstantiationByFactoryMethod == userInstantiationByFactoryBean2); //false 创造的是不同的对象
        */

        ServiceLoader<UserFactory> serviceLoader = applicationContext.getBean("userServiceLoaderFactoryBean", ServiceLoader.class);

        display(serviceLoader);
        serviceLoaderTest();

        //通过 AutowireCapableBeanFactory 去创建Bean
        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
        DefaultUserFactory factoryBean = beanFactory.getBean(DefaultUserFactory.class);
        System.out.println(factoryBean.createUser()); //User {id=2, name='张三'}

    }


    public static void serviceLoaderTest(){
        ServiceLoader<UserFactory> serviceLoader = ServiceLoader.load(UserFactory.class, Thread.currentThread().getContextClassLoader());
        display(serviceLoader);
    }


    private static void display(ServiceLoader<UserFactory> serviceLoader){
        Iterator<UserFactory> iterator = serviceLoader.iterator();
        while (iterator.hasNext()){
            UserFactory userFactory = iterator.next();
            System.out.println(userFactory.createUser());
        }
    }
}
