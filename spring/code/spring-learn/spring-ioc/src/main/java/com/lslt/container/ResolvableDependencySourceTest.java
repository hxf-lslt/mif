package com.lslt.container;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author lslt
 * @description
 *  ConfigurableListableBeanFactory 中  registerResolvableDependency
 *  通过这种方式注册的 bean 只能用于依赖注入不能用于依赖查找 而且只能通过类型来注入
 * @date 2024/6/3 23:52
 */
@Configurable
public class ResolvableDependencySourceTest {

    @Autowired
    private String value;


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(ResolvableDependencySourceTest.class);

        // 添加一个回调
        // 会在 refresh 中 invokeBeanFactoryPostProcessors() 方法中执行注册实例对象
        // 会优先于 @Autowired 修饰的依赖注入 finishBeanFactoryInitialization() 在这个方法里面执行
        // 可以查看 AbstractApplicantContext 中的 refresh() 里面的执行顺序
        applicationContext.addBeanFactoryPostProcessor(beanFactory -> {
            beanFactory.registerResolvableDependency(String.class,"Hello,World!");
        });

        applicationContext.refresh();

        ResolvableDependencySourceTest bean = applicationContext.getBean(ResolvableDependencySourceTest.class);
        System.out.println(bean.value);

        applicationContext.close();
    }
}
