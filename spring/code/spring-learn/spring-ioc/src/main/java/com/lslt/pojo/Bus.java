package com.lslt.pojo;

/**
 * @author lslt
 * @description
 * @date 2024/6/4 23:29
 */
public class Bus extends Vehicle{

    private Integer seat;


    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "seat=" + seat +
                "} " + super.toString();
    }
}
