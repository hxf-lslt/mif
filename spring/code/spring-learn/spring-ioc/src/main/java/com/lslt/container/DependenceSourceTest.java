package com.lslt.container;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.io.ResourceLoader;

import javax.annotation.PostConstruct;

/**
 * @author lslt
 * @description 测试依赖注入来源
 * <p>
 * <p>
 * 可以查看 AbstractApplicationContext 中的 prepareBeanFactory() 方法
 * beanFactory.registerResolvableDependency(BeanFactory.class, beanFactory);
 * beanFactory.registerResolvableDependency(ResourceLoader.class, this);
 * beanFactory.registerResolvableDependency(ApplicationEventPublisher.class, this);
 * beanFactory.registerResolvableDependency(ApplicationContext.class, this);
 * <p>
 * 使用 ResolvableDependency 只能被依赖注入无法被依赖查找
 * @date 2024/6/3 20:29
 */
@Configurable
public class DependenceSourceTest {

    @Autowired
    private BeanFactory beanFactory;
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;
    @Autowired
    private ApplicationContext applicationContext;


    @PostConstruct
    public void init() {
        System.out.println("beanFactory == applicationContext: " + (beanFactory == applicationContext)); //false
        System.out.println("beanFactory == applicationContext.getBeanFactory(): " + (beanFactory == applicationContext.getAutowireCapableBeanFactory())); //true
        System.out.println("resourceLoader == applicationContext: " + (resourceLoader == applicationContext)); //true
        System.out.println("applicationEventPublisher == applicationContext: " + (applicationEventPublisher == applicationContext)); //true
    }

    @PostConstruct
    public void initByLookup() {
        //这 4 个 Bean 均无法通过 beanFactory 查找到.都会报 NoSuchBeanDefinitionException 错误
        //无法通过依赖查找来找到具体的 Bean
        getBeanByType(BeanFactory.class);
        getBeanByType(ResourceLoader.class);
        getBeanByType(ApplicationEventPublisher.class);
        getBeanByType(ApplicationContext.class);
    }

    public <T> T getBeanByType(Class<T> beanType) {
        try {
            return beanFactory.getBean(beanType);
        } catch (NoSuchBeanDefinitionException e) {
            System.err.println("无法通过 BeanFactory 查找到：" + beanType.getName());
        }
        return null;
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(DependenceSourceTest.class);

        //启动容器
        applicationContext.refresh();

        //获取 bean
        DependenceSourceTest bean = applicationContext.getBean(DependenceSourceTest.class);
        System.out.println(bean.applicationContext == applicationContext); //true
        applicationContext.close();
    }
}
