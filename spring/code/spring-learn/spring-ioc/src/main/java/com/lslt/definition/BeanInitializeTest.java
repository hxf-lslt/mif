package com.lslt.definition;

import com.lslt.factory.DefaultUserFactory;
import com.lslt.factory.UserFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author lslt
 * @description
 * @date 2024/5/26 19:51
 */
public class BeanInitializeTest {


    /**
     * <p>初始化 Bean<p/>
     * <p>1. @PostStruct 注解<p/>
     * <p>2. 实现 {@link InitializingBean#afterPropertiesSet()}
     * 方法<p/>
     * <p>3. @Bean 添加 initMethod 属性 <p/>
     * <br />
     * <p>销毁 Bean<p/>
     * <p>1. @PreDestroy 注解<p/>
     * <p>2. 实现 {@link DisposableBean#destroy()} 方法<p/>
     * <p>3. @Bean 添加 destroyMethod 属性 <p/>
     */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        //注册
        applicationContext.register(BeanInitializeTest.class);
        //开启容器
        applicationContext.refresh();

        UserFactory userFactory = applicationContext.getBean(UserFactory.class);
        //关闭
        applicationContext.close();
    }


    @Bean(initMethod = "initMethod",destroyMethod = "destroyMethod")
    public UserFactory userFactory(){
        return new DefaultUserFactory();
    }
}
