package com.lslt.container;

import com.lslt.pojo.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Map;

/**
 * @author lslt
 * @description 使用注解方式 {@link ApplicationContext} 实现容器
 * @date 2024/5/25 15:57
 */
public class AnnotationApplicationContextAsIocContainer {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(AnnotationApplicationContextAsIocContainer.class);
        applicationContext.refresh(); //如果不调用 refresh 会报错rg.springframework.context.annotation.AnnotationConfigApplicationContext@6debcae2 has not been refreshed yet
        lookupCollection(applicationContext); // {user=User{id=2, name='李桑'}}
    }


    /*
     * 使用注解的方式定义 bean
     */
    @Bean
    public User user(){
        User user = new User();
        user.setId(2L);
        user.setName("李桑");
        return user;
    }

    private static void lookupCollection(BeanFactory beanFactory){
        if(beanFactory instanceof ListableBeanFactory){
            ListableBeanFactory listableBeanFactory = (ListableBeanFactory)beanFactory;
            Map<String, User> beansOfType = listableBeanFactory.getBeansOfType(User.class);
            System.out.println(beansOfType);
        }
    }

}
