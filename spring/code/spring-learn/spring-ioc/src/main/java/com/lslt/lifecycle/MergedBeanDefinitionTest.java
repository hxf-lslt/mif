package com.lslt.lifecycle;

import com.lslt.pojo.Bus;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

/**
 * @author lslt
 * @description
 * @date 2024/6/5 1:08
 */
public class MergedBeanDefinitionTest {

    public static void main(String[] args) {

        // DefaultListableBeanFactory 实现了 BeanDefinitionRegister
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        //加载资源
        reader.loadBeanDefinitions("classpath:/bean-container.xml");
        //依赖查找
        Bus bus = beanFactory.getBean("bus", Bus.class);
        System.out.println(bus);


    }
}
