package com.lslt.definition;

import com.lslt.pojo.User;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * @author lslt
 * @description
 * @date 2024/5/26 21:13
 */
public class DependencyLookupTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(DependencyLookupTest.class);
        applicationContext.refresh();
//        lookupByBeanProvider(applicationContext);



        //测试获取 Bean 的方法的安全性
        testBeanFactory(applicationContext); //org.springframework.beans.factory.NoSuchBeanDefinitionException

        testObjectFactory(applicationContext); //org.springframework.beans.factory.NoSuchBeanDefinitionException

        testObjectProvider(applicationContext);
    }


    @Bean
    public String helloWorld() {
        return "hello,World!";
    }


    //ObjectProvider 继承了 ObjectFactory
    public static void lookupByBeanProvider(AnnotationConfigApplicationContext applicationContext) {
        ObjectProvider<String> beanProvider = applicationContext.getBeanProvider(String.class);
        System.out.println(beanProvider.getObject()); //hello,World!
    }

    //////////////////////测试安全性/////////////////////////////////

    // 1. BeanFactory#getBean() 是不安全的
    public static void testBeanFactory(AnnotationConfigApplicationContext applicationContext){
        printBeanException("BeanFactory#getBean()",()->applicationContext.getBean(User.class));
    }

    //2. ObjectFactory#getObject() 是不安全的
    public static void testObjectFactory(AnnotationConfigApplicationContext applicationContext){
        printBeanException("ObjectFactory#getObject()",()->applicationContext.getBeanProvider(User.class).getObject());
    }

    //3. ObjectProvider#getIfAvailable()  是安全的
    public static void testObjectProvider(AnnotationConfigApplicationContext applicationContext){
        printBeanException("ObjectProvider#getIfAvailable()",()->applicationContext.getBeanProvider(User.class).getIfAvailable());
    }



    //打印日志
    public static void printBeanException(String source,Runnable runnable){
        System.err.println("信息来源：" + source);
        try {
            runnable.run();
        } catch (BeansException e) {
            e.printStackTrace();
        }
    }
}
