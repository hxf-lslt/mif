package com.lslt.lifecycle;

import com.lslt.pojo.User;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.PropertiesBeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;

/**
 * @author lslt
 * @description Bean 元数据信息配置测试
 * @date 2024/6/4 21:12
 */
public class BeanMetadataConfigurationTest {

    public static void main(String[] args) {
        BeanMetadataConfigurationTest test = new BeanMetadataConfigurationTest();
//        test.xmlBeanMetadataConfiguration();
//        test.propertiesBeanMetadataConfiguration();
        test.annotatedBeanMetadataConfiguration();
    }



    //2.1 使用注解方式 AnnotatedBeanDefinitionReader
    public void annotatedBeanMetadataConfiguration(){
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        AnnotatedBeanDefinitionReader reader = new AnnotatedBeanDefinitionReader(beanFactory);
        int beanDefinitionCountBefore = beanFactory.getBeanDefinitionCount();
        //可以注册多个 bean
//        reader.register(BeanMetadataConfigurationTest.class);
        //不需要类上使用 @Component 等注解
        reader.register(BeanMetadataConfigurationTest.class,User.class);
        int beanDefinitionCountAfter = beanFactory.getBeanDefinitionCount();
        // bean 的名字由 AnnotationBeanNameGenerator 生成 默认的方法 buildDefaultBeanName 就是将类名首字母小写
        BeanMetadataConfigurationTest bean = beanFactory.getBean("beanMetadataConfigurationTest", BeanMetadataConfigurationTest.class);
        System.out.println("注册了 Bean 的数量：" + (beanDefinitionCountAfter - beanDefinitionCountBefore));
        System.out.println(bean);
        System.out.println(beanFactory.getBean("user"));
    }


    // 1.1. 使用 xml 方式
    // 通过 XmlBeanDefinitionReader 加载资源
    public void xmlBeanMetadataConfiguration(){
        // DefaultListableBeanFactory 实现了 BeanDefinitionRegister
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);
        //加载资源
        reader.loadBeanDefinitions("classpath:/bean-container.xml");
        //依赖查找
        User user = beanFactory.getBean("user", User.class);
        System.out.println(user);
    }

    // 1.2. 通过 properties 文件加载资源
    // PropertiesBeanDefinitionReader 已经不建议使用了
    public void propertiesBeanMetadataConfiguration(){
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        PropertiesBeanDefinitionReader reader = new PropertiesBeanDefinitionReader(beanFactory);
        reader.loadBeanDefinitions("classpath:/user.properties");
        User user = beanFactory.getBean("user", User.class);
        System.out.println(user);
    }
}
