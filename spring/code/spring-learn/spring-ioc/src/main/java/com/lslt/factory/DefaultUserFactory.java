package com.lslt.factory;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * @author lslt
 * @description
 * @date 2024/5/26 14:01
 */
public class DefaultUserFactory implements UserFactory, InitializingBean, DisposableBean {


    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct: 初始化调用");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("InitializingBean#afterPropertiesSet: 初始化调用");
    }

    public void initMethod() {
        System.out.println("自定义的初始化方法....");
    }


    @PreDestroy
    public void preDestroy() {
        System.out.println("@PreDestroy 销毁方法调用...");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("DisposableBean#destroy() 销毁调用");
    }

    public void destroyMethod() {
        System.out.println("自定义销毁方法....");
    }
}
