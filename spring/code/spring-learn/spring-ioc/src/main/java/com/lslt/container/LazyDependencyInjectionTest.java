package com.lslt.container;

import com.lslt.pojo.User;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Set;

/**
 * @author lslt
 * @description 延迟依赖注入测试
 *   单个类型和集合类型的都可以延迟依赖注入
 * @date 2024/6/2 17:09
 */
public class LazyDependencyInjectionTest {



    @Autowired
    private User user;

    @Autowired
    private ObjectProvider<User> objectProvider;

    @Autowired
    private ObjectProvider<Set<User>> usersObjectFactory;


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext();

        applicationContext.register(LazyDependencyInjectionTest.class);

        //xmlBeanDefinition 解析器 读取配置文件
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(applicationContext);
        //加载 BeanDefinition
        String resourcePath = "classpath:/dependence-injection.xml";
        xmlBeanDefinitionReader.loadBeanDefinitions(resourcePath);

        //启动容器
        applicationContext.refresh();


        //获取 bean
        LazyDependencyInjectionTest bean = applicationContext.getBean(LazyDependencyInjectionTest.class);

        System.out.println(bean.user); //SuperUser{address='上海'} User{id=1, name='张三'}
        System.out.println(bean.objectProvider.getObject()); //SuperUser{address='上海'} User{id=1, name='张三'}

        bean.objectProvider.forEach(System.out::println);  //User{id=1, name='张三'}
                                                           //SuperUser {address='上海'} User{id=1, name='张三'}

        System.out.println(bean.usersObjectFactory.getObject()); //[User{id=1, name='张三'}, SuperUser{address='上海'} User{id=1, name='张三'}]

        applicationContext.close();
    }
}
