package com.lslt.pojo;

import com.lslt.annotation.Super;

/**
 * @author lslt
 * @description
 * @date 2024/5/22 23:17
 */

@Super
public class SuperUser extends User{

    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "SuperUser{" +
                "address='" + address + '\'' +
                "} " + super.toString();
    }
}
