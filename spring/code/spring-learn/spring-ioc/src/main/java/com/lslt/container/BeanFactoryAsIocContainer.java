package com.lslt.container;

import com.lslt.pojo.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;

import java.util.Map;

/**
 * @author lslt
 * @description 使用 {@link BeanFactory} 作为 IOC 容器
 * @date 2024/5/25 15:47
 */
public class BeanFactoryAsIocContainer {



    public static void main(String[] args) {
        //创建 BeanFactory 容器
        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        // DefaultListableBeanFactory 实现了 BeanDefinitionRegistry 具有加载 BeanDefinition 的能力
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);

        //加载 Bean 资源
        reader.loadBeanDefinitions("classpath:/bean-container.xml");

        lookupCollection(beanFactory); //{user=User{id=1, name='张三'}, superUser=SuperUser{address='上海'} User{id=1, name='张三'}}
    }



    private static void lookupCollection(BeanFactory beanFactory){
        if(beanFactory instanceof ListableBeanFactory){
            ListableBeanFactory listableBeanFactory = (ListableBeanFactory)beanFactory;
            Map<String, User> beansOfType = listableBeanFactory.getBeansOfType(User.class);
            System.out.println(beansOfType);
        }
    }

}
