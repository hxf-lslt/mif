package com.lslt.annotation;

import java.lang.annotation.*;

/**
 * @author lslt
 * @description
 * @date 2024/6/3 19:30
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface InjectedUser {
}
