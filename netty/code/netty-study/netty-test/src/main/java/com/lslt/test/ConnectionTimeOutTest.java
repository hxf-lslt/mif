package com.lslt.test;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.net.InetSocketAddress;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 14:38
 */


public class ConnectionTimeOutTest {

    public static void main(String[] args) {
        try {
            ChannelFuture future = new Bootstrap()
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS,1000)
                    .group(new NioEventLoopGroup())
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {

                        }
                    }).connect(new InetSocketAddress("localhost", 8080));

            future.sync().channel().closeFuture().sync();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
