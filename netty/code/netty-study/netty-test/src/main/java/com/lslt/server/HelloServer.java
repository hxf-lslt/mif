package com.lslt.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lslt
 * @description
 * @date 2024/5/5 14:53
 */
@Slf4j
public class HelloServer {

    public static void main(String[] args) {
        new ServerBootstrap()
                .group(new NioEventLoopGroup())
//                .option(ChannelOption.SO_RCVBUF,2)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel channel) throws Exception {
//                channel.pipeline().addLast(new StringDecoder());
//                channel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
//                    @Override
//                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//                        log.info("接收到的消息：{}",msg);
//                    }
//                });
                channel.pipeline().addLast(new FixedLengthFrameDecoder(10));
                channel.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
            }
        }).bind(8080);
    }
}
