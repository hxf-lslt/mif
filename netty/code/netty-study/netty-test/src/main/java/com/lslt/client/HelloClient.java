package com.lslt.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;
import java.util.Scanner;

/**
 * @author lslt
 * @description
 * @date 2024/5/5 14:59
 */
@Slf4j
public class HelloClient {

    public static void main(String[] args) throws InterruptedException {
        testCoalescing();
    }


    /**
     * 测试粘包和半包现象
     */
    public static void testCoalescing() throws InterruptedException {
        ChannelFuture channelFuture = new Bootstrap()
                .group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel channel) throws Exception {
                        channel.pipeline().addLast(new LoggingHandler());
                        channel.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                            @Override
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                ByteBuf buf = ctx.alloc().buffer();
                                char c = '0';
                                Random r = new Random();
                                for (int i = 0; i < 10; i++) {
                                    buf.writeBytes(filedBytes(c,r.nextInt(10) + 1));
                                    c++;
                                }
                                ctx.writeAndFlush(buf);
                            }
                        });
                    }
                }).connect("localhost", 8080);
        Channel channel = channelFuture.sync().channel();
    }

    public static byte[] filedBytes(char c,int charLen){
        byte[] bytes = new byte[10];
        for (int i = 0; i < bytes.length; i++) {
            if(i < charLen){
                bytes[i] = (byte)c;
            }else{
                bytes[i] = (byte)'_';
            }
        }
        return bytes;
    }

    public static void testChannelClose() throws InterruptedException {
        NioEventLoopGroup group = new NioEventLoopGroup();
        ChannelFuture channelFuture = new Bootstrap()
                .group(group)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel channel) throws Exception {
                        channel.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
                        channel.pipeline().addLast(new StringEncoder());
                    }
                }).connect("localhost", 8080);
        Channel channel = channelFuture.sync().channel();

        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String line = scanner.nextLine();
                if ("q".equalsIgnoreCase(line)) {
                    channel.close(); //close是一个异步的操作
//                    log.debug("处理关闭后的事情"); //在这处理关闭后的事情是不行的
                    break;
                }
                channel.writeAndFlush(line);
            }
        }, "input").start();

        //解决办法
        //1.同步等待
        /*
            ChannelFuture closeFuture = channel.closeFuture();
            closeFuture.sync();
            log.debug("处理关闭后的事情");
        */

        //2.添加监听器，异步解决
        ChannelFuture closeFuture = channel.closeFuture();
        closeFuture.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                log.debug("处理关闭后的事情");
                group.shutdownGracefully();
            }
        });

    }

    public static void testChannelFuture() {
        //返回的是一个异步对象
        ChannelFuture channelFuture = new Bootstrap().group(new NioEventLoopGroup()).channel(NioSocketChannel.class).handler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel channel) throws Exception {
                channel.pipeline().addLast(new StringEncoder());
            }
        })
                //connect 是一个异步非阻塞的方法，使用NioEventLoopGroup中的一个线程去建立连接
                .connect("localhost", 8080);

        //2.1同步等待，连接建立完毕，如果不使用sync,主线程会一直往下执行，有可能还没以后建立好连接，就不可能拿到channel对象，返送消息
//        channelFuture.sync();
//        Channel channel = channelFuture.channel();
//        log.debug("channel:{}",channel);
//        channel.writeAndFlush("123");
        //2.2还有一个方法可以 ChannelFuture.addListener,添加一个监听器，等连接建立完毕后，调用回调函数
        channelFuture.addListener(new ChannelFutureListener() {
            @Override
            //建立连接后回调
            public void operationComplete(ChannelFuture future) throws Exception {
                Channel channel = future.channel();
                log.debug("channel:{}", channel);
                channel.writeAndFlush("123");
            }
        });
    }

    public static void bootStrap() throws InterruptedException {
        new Bootstrap().group(new NioEventLoopGroup()).channel(NioSocketChannel.class).handler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel channel) throws Exception {
                channel.pipeline().addLast(new StringEncoder());
            }
        }).connect("localhost", 8080).sync().channel().writeAndFlush("hello");
    }
}
