package com.lslt.test;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author lslt
 * @description
 * @date 2024/5/5 22:25
 */
@Slf4j
public class EventLoopGroupTest {

    public static void main(String[] args) {
        //创建NIO可以处理 IO事件、定时任务、普通任务
        //构造函数可以传线程数，不传默认为你电脑的核心线程的2倍
        EventLoopGroup group = new NioEventLoopGroup(2);

        //查看核心线程数 16
//        System.out.println(NettyRuntime.availableProcessors());

        // 1.next() 方法获取 NioEventLoop 对象
/*        System.out.println(group.next());
        System.out.println(group.next());
        System.out.println(group.next());
        System.out.println(group.next());*/

        // 2. 执行普通任务
    /*    group.next().submit(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            log.debug("我在执行普通任务");
        });*/

        /**
         * 3.执行定时任务
         * 参数：
         *      1.要执行的任务
         *      2.什么时候执行
         *      3.每间隔多少时间执行
         *      4.间隔时间单位
         */
        group.next().scheduleAtFixedRate(()->{
            log.debug("我在执行定时任务");
        },0,1, TimeUnit.SECONDS);

        log.debug("main");
    }
}
