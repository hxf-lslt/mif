package com.lslt.test;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.*;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

/**
 * @author lslt
 * @description
 * @date 2024/5/8 23:00
 */
@Slf4j
public class TestHttp {
    public static void main(String[] args) throws InterruptedException {
        ChannelFuture future = new ServerBootstrap()
                .group(new NioEventLoopGroup())
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel channel) {
                        channel.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG));
                        //http协议
                        channel.pipeline().addLast(new HttpServerCodec());
                        channel.pipeline().addLast(new SimpleChannelInboundHandler<HttpRequest>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext chx, HttpRequest request) {
                                log.info("{}",request.uri());

                                //返回给浏览器消息
                                DefaultFullHttpResponse response = new DefaultFullHttpResponse(request.protocolVersion(), HttpResponseStatus.OK);
                                byte[] bytes = "<h1>Hello, World!".getBytes(StandardCharsets.UTF_8);
                                int length = bytes.length;
                                response.headers().setInt(HttpHeaderNames.CONTENT_LENGTH,length);
                                response.content().writeBytes(bytes);
                                chx.channel().writeAndFlush(response);
                            }
                        });
                    }
                }).bind(8080);


        future.channel().closeFuture().sync();
    }
}
