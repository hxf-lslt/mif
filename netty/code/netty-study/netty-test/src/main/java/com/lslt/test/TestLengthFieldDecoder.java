package com.lslt.test;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;

import java.nio.charset.StandardCharsets;

/**
 * @author lslt
 * @description
 * @date 2024/5/8 22:17
 */
public class TestLengthFieldDecoder {

    public static void main(String[] args) {
        EmbeddedChannel channel = new EmbeddedChannel();

        /**
         * LengthFieldBasedFrameDecoder中的5个参数
         *  1.maxFrameLength 最大帧长数
         *  2.lengthFieldOffset 记录内容长度的开始位置
         *  3.lengthFieldLength 内容长度的字节数
         *  4.lengthAdjustment 从内容长度到内容要偏移几个字节数
         *  5.initialBytesToStrip 跳过几个字节开始读取内容
         */
        channel.pipeline().addLast(new LengthFieldBasedFrameDecoder(512,0,4,1,0));
        channel.pipeline().addLast(new LoggingHandler());

        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        send(buf, "Hello, world");
        send(buf, "Hi!");
        channel.writeInbound(buf);
    }

    private static void send(ByteBuf buf, String content) {
        byte[] bytes = content.getBytes(StandardCharsets.UTF_8);
        int length = bytes.length;
        //4个字节的内容长度
        buf.writeInt(length);
        //1个字节的版本号
        buf.writeByte(0);
        //内容
        buf.writeBytes(bytes);
    }
}
