package com.lslt.nio.server;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author lslt
 * @description 测试 NIO 服务器
 * @date 2024/5/2 0:49
 */

@Slf4j
public class Server {


    public static void main(String[] args) throws IOException {

        nonBlocking();
    }


    public static void nonBlocking() throws IOException {

        //创建服务器Channel
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //设置非阻塞
        ssc.configureBlocking(false);
        //绑定端口
        ssc.bind(new InetSocketAddress(8080));

        //建立selector
        Selector selector = Selector.open();

        //将channel注册到selector中
        SelectionKey sscKey = ssc.register(selector, 0, null);

        //关注感兴趣的事件（accept事件）
        sscKey.interestOps(SelectionKey.OP_ACCEPT);
        log.info("注册的事件：{}", sscKey);

        while (true) {
            //selector 监听事件 阻塞，等待事件的发生
            //如果有事件未处理时 则不会阻塞
            //事件要么被处理，要么取消。  key.cancel();
            selector.select();

            //拿到selector中的所有事件
            Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();

                //如果是连接事件，
                if (key.isAcceptable()) {
                    log.info("触发事件的key:{}", key);
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    //上面只是注册了accept事件(客户端发生连接)
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    ByteBuffer buffer = ByteBuffer.allocate(16);
                    //每一个channel绑定一个Buffer
                    SelectionKey scKey = sc.register(selector, 0, buffer);
                    //关注读事件
                    scKey.interestOps(SelectionKey.OP_READ);
                    log.info("连接的客户端：{}", sc);
                } else if (key.isReadable()) {
                    try {
                        SocketChannel channel = (SocketChannel) key.channel();
                        ByteBuffer buffer = (ByteBuffer)key.attachment();
                        int read = channel.read(buffer);
                        //说明客户端关闭连接
                        if(read == -1){
                            key.cancel();
                        }
                        split(buffer);
                        //当position的位置和limit的位置一样时，说明没有读取Buffer内容（没有遇到分割符），需要扩容
                      if(buffer.position() == buffer.limit()){
                          ByteBuffer newBuffer = ByteBuffer.allocate(buffer.capacity() * 2);
                          buffer.flip();
                          newBuffer.put(buffer);
                          key.attach(newBuffer);
                          buffer = null;
                      }
                    } catch (IOException e) {
                        e.printStackTrace();
                        key.cancel();
                    }

                }
                iter.remove(); //selector 处理完事件后，不会从事件集合中移除。需要自己手动删除。
            }
        }
    }


    public static void split(ByteBuffer source){
        //切换为读模式
        source.flip();
        for (int i = 0; i < source.limit(); i++) {

            //如果读取到一条消息结束符
            if('\n' == source.get(i)){
                int len = i  - source.position();
                ByteBuffer target = ByteBuffer.allocate(len);
                for (int j = 0; j < len; j++) {
                    target.put(source.get());
                }
                //将\n丢弃，如果不将\n丢弃，计算 len = i + 1 - source.position 同时source.get()不需要调用
                source.get();
                target.flip();
                byte[] buf = new byte[len];
                target.get(buf,0,len);
                log.debug("读取的数据：{}",new String(buf));
            }
        }

        //切换为写模式
        source.compact();
    }


    public static void blocking() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(16);

        //创建服务器Channel
        ServerSocketChannel ssc = ServerSocketChannel.open();

        //设置非阻塞
        ssc.configureBlocking(false);

        //绑定端口
        ssc.bind(new InetSocketAddress(8080));

        List<SocketChannel> channels = new ArrayList<>();

        while (true) {
            //等待客户端的连接
//            log.info("waiting connected....");
            SocketChannel sc = ssc.accept();  //阻塞方法

            if (null != sc) {
                log.info("connected: {}", sc);
                sc.configureBlocking(false); //设置为非阻塞模式
                channels.add(sc);

            }
            //处理每条 Channel 中的消息
            for (SocketChannel channel : channels) {
                //接受读取消息
//                log.info("before read....{}", channel);
                int read = channel.read(buffer);//阻塞。线程不能往下执行
                if (read > 0) {
                    buffer.flip();
                    byte[] buf = new byte[buffer.limit()];
                    buffer.get(buf, 0, buffer.limit());
                    log.debug("接收到的消息：{}", new String(buf));
                    buffer.clear();
                    log.info("after read....{}", channel);
                }
            }
        }
    }
}
