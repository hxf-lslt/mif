package com.lslt.nio.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

/**
 * @author lslt
 * @description 客户端
 * @date 2024/5/2 0:59
 */
public class Client {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();

        sc.connect(new InetSocketAddress("localhost",8080));
        sc.write(StandardCharsets.UTF_8.encode("hello"));
        System.out.println(System.in.read());

    }
}
