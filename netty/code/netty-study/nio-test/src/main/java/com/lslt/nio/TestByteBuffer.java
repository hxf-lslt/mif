package com.lslt.nio;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author lslt
 * @description 测试ByteBuffer
 * @date 2024/5/1 21:08
 */


@Slf4j
public class TestByteBuffer {


    public static void main(String[] args) {
        testFragmentation();
    }


    /**
     * 测试byteBuffer
     */
    public static void testByteBuffer(){
        //使用 FileChannel 读取文件
        try (FileChannel channel = new FileInputStream("data.txt").getChannel()) {
            //将文件通过 Channel 读取到 ByteBuffer
            ByteBuffer buffer = ByteBuffer.allocate(10);
            int len ;
            while ((len = channel.read(buffer)) > 0){

                log.debug("读取字节数：{}",len);
                buffer.flip(); //切换模式
                while(buffer.hasRemaining()){
                    //读取一个字节
                    byte b = buffer.get();
                    log.debug("读取的字符是：{}",(char)b);
                }
                buffer.clear(); //清除读位置
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 模拟黏包和半包的问题
     * 假设每条消息以 \n 作为结束标志
     */
    public static void testFragmentation(){
        ByteBuffer source = ByteBuffer.allocate(32);
        source.put("Hello,world\nI'm lisi\nHo".getBytes());
        split(source);
        source.put("w are you\n".getBytes());
        split(source);
    }

    public static void split(ByteBuffer source){
        //切换为读模式
        source.flip();
        for (int i = 0; i < source.limit(); i++) {

            //如果读取到一条消息结束符
            if('\n' == source.get(i)){
                int len = i  - source.position();
                ByteBuffer target = ByteBuffer.allocate(len);
                for (int j = 0; j < len; j++) {
                    target.put(source.get());
                }
                //将\n丢弃，如果不将\n丢弃，计算 len = i + 1 - source.position 同时source.get()不需要调用
                source.get();
                target.flip();
                byte[] buf = new byte[len];
                target.get(buf,0,len);
                log.debug("读取的数据：{}",new String(buf));
            }
        }

        //切换为写模式
        source.compact();
    }
}
