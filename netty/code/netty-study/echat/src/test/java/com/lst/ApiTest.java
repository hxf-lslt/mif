package com.lst;

import com.lslt.codec.MessageCodec;
import com.lslt.message.LoginRequestMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.embedded.EmbeddedChannel;
import io.netty.handler.logging.LoggingHandler;
import org.junit.Test;

/**
 * @author lslt
 * @description
 * @date 2024/5/9 22:36
 */
public class ApiTest {

    @Test
    public void testMessageCodec() throws Exception {
        EmbeddedChannel channel = new EmbeddedChannel(new LoggingHandler(),new MessageCodec());
//        channel.pipeline().addLast();

        LoginRequestMessage loginRequestMessage = new LoginRequestMessage();

        //encode
//        channel.writeOutbound(loginRequestMessage);

        //decode
        ByteBuf buf = channel.alloc().buffer();
        new MessageCodec().encode(null,loginRequestMessage,buf);
        channel.writeInbound(buf);
    }
}
