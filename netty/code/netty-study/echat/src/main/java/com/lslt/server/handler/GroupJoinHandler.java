package com.lslt.server.handler;

import com.lslt.message.GroupJoinRequestMessage;
import com.lslt.message.GroupJoinResponseMessage;
import com.lslt.message.GroupMembersResponseMessage;
import com.lslt.server.session.Group;
import com.lslt.server.session.GroupSession;
import com.lslt.server.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:44
 */
public class GroupJoinHandler extends SimpleChannelInboundHandler<GroupJoinRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupJoinRequestMessage message) throws Exception {
        GroupSession groupSession = GroupSessionFactory.getGroupSession();
        if(groupSession.isGroupExist(message.getGroupName())){
            Group group = groupSession.joinMember(message.getGroupName(), message.getFrom());
            if(null != group){
                ctx.channel().writeAndFlush(new GroupJoinResponseMessage(true,"您已加入群聊["+message.getGroupName() + "]和大家打声招呼吧！"));
                List<Channel> membersChannel = groupSession.getMembersChannel(message.getGroupName());
                for (Channel channel : membersChannel) {
                    if(channel == ctx.channel()){
                        continue;
                    }
                    channel.writeAndFlush(new GroupJoinResponseMessage(true, message.getFrom()+"已加入群聊"));
                }
            }else{
                ctx.channel().writeAndFlush(new GroupJoinResponseMessage(false,"加入失败"));
            }
        }else {
            ctx.channel().writeAndFlush(new GroupMembersResponseMessage(false,"群名不存在"));
        }

    }
}

