package com.lslt.server.session;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:24
 */
public abstract class GroupSessionFactory {

    private static GroupSession session = new GroupSessionMemoryImpl();

    public static GroupSession getGroupSession() {
        return session;
    }
}