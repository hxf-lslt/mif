package com.lslt.server.service;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:15
 */
public abstract class UserServiceFactory {

    private static UserService userService = new UserServiceMemoryImpl();

    public static UserService getUserService() {
        return userService;
    }
}