package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:08
 */
public class GroupChatRequestMessage extends AbstractMessage {

    /**
     * 发送者
     */
    private String from;

    /**
     * 内容
     */
    private String content;

    /**
     * 群名
     */
    private String groupName;


    public GroupChatRequestMessage(String from, String groupName, String content) {
        this.from = from;
        this.content = content;
        this.groupName = groupName;
    }

    @Override
    public int getMessageType() {
        return 6;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
