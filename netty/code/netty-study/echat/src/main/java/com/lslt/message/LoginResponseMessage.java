package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 23:18
 */
public class LoginResponseMessage extends AbstractResponseMessage {

    public LoginResponseMessage(boolean status, String info) {
        super(status, info);
    }

    @Override
    public int getMessageType() {
        return 1;
    }

    @Override
    public String toString() {
        return "LoginResponseMessage{" +
                "status=" + status +
                ", info='" + info + '\'' +
                '}';
    }
}
