package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:29
 */
public class GroupMembersResponseMessage extends AbstractResponseMessage{


    /**
     * 成员
     */
    private String members;


    public GroupMembersResponseMessage(String members) {
        this(true,"查询成功");
        this.members = members;
    }

    public GroupMembersResponseMessage(boolean status, String info) {
        super(status, info);
    }

    @Override
    public String toString() {
        return "GroupMembersResponseMessage{" +
                "status=" + status +
                ", info='" + info + '\'' +
                ", members='" + members + '\'' +
                '}';
    }

    @Override
    public int getMessageType() {
        return 9;
    }
}
