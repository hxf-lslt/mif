package com.lslt.server.session;

import java.util.Collections;
import java.util.Set;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:20
 */
public class Group {
    // 聊天室名称
    private String name;
    // 聊天室成员
    private Set<String> members;

    public static final Group EMPTY_GROUP = new Group("empty", Collections.emptySet());

    public Group(String name, Set<String> members) {
        this.name = name;
        this.members = members;
    }

    public Set<String> getMembers(){
        return this.members;
    }
}