package com.lslt.codec;

import com.google.gson.Gson;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 13:38
 */
public interface Serializer {


    /**
     * 将字节数组反序列化成对象
     *
     * @param clazz Class
     * @param bytes 字节数组
     * @param <T>   类型
     * @return 对象
     */
    <T> T deserialize(Class<T> clazz, byte[] bytes);


    /**
     * 将序列化为字节数组
     *
     * @param obj 对象
     * @param <T> 类型
     * @return 字节数组
     */
    <T> byte[] serialize(T obj);


    enum Algorithm implements Serializer {

        /**
         * jdk方式
         */
        JAVA {
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                try {
                    ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
                    ObjectInputStream ois = new ObjectInputStream(bis);
                    return (T) ois.readObject();
                } catch (IOException | ClassNotFoundException e) {
                    throw new RuntimeException("反序列化失败{}", e);
                }

            }

            @Override
            public <T> byte[] serialize(T obj) {
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(bos);
                    oos.writeObject(obj);
                    return bos.toByteArray();
                } catch (IOException e) {
                    throw new RuntimeException("序列化失败{}", e);
                }
            }
        },
        JSON{
            @Override
            public <T> T deserialize(Class<T> clazz, byte[] bytes) {
                String json = new String(bytes, StandardCharsets.UTF_8);
                return new Gson().fromJson(json,clazz);
            }

            @Override
            public <T> byte[] serialize(T obj) {
                String json = new Gson().toJson(obj);
                return json.getBytes(StandardCharsets.UTF_8);
            }
        }
    }
}
