package com.lslt.codec;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:48
 */
public class ProtocolFrameDecoder extends LengthFieldBasedFrameDecoder {

    public ProtocolFrameDecoder(){
        this(1024,12,4,0,0);
    }

    public ProtocolFrameDecoder( int maxFrameLength,
                                 int lengthFieldOffset, int lengthFieldLength,
                                 int lengthAdjustment, int initialBytesToStrip) {
        super(maxFrameLength, lengthFieldOffset, lengthFieldLength,lengthAdjustment,initialBytesToStrip);
    }

}
