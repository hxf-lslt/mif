package com.lslt.server.handler;

import com.lslt.message.GroupJoinResponseMessage;
import com.lslt.message.GroupQuitRequestMessage;
import com.lslt.message.GroupQuitResponseMessage;
import com.lslt.server.session.Group;
import com.lslt.server.session.GroupSession;
import com.lslt.server.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:44
 */
public class GroupQuitHandler extends SimpleChannelInboundHandler<GroupQuitRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupQuitRequestMessage message) throws Exception {
        GroupSession groupSession = GroupSessionFactory.getGroupSession();
        if(groupSession.isGroupExist(message.getGroupName())){
            Group group = groupSession.removeMember(message.getGroupName(), message.getFrom());
            if(null != group){
                ctx.channel().writeAndFlush(new GroupQuitResponseMessage(true,"您退出群聊["+message.getGroupName() + "]"));
                List<Channel> membersChannel = groupSession.getMembersChannel(message.getGroupName());
                if(groupSession.getMembers(message.getGroupName()).size() == 0){
                    groupSession.removeGroup(message.getGroupName());
                }
                for (Channel channel : membersChannel) {
                    channel.writeAndFlush(new GroupJoinResponseMessage(true, message.getFrom()+"已退出群聊"));
                }
            }else{
                ctx.channel().writeAndFlush(new GroupQuitResponseMessage(false,"退出失败"));
            }
        }else {
            ctx.channel().writeAndFlush(new GroupQuitResponseMessage(false,"群名不存在"));
        }

    }
}

