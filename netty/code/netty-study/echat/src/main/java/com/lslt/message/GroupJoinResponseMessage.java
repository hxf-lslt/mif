package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:40
 */
public class GroupJoinResponseMessage extends AbstractResponseMessage{


    public GroupJoinResponseMessage(boolean status, String info) {
        super(status, info);
    }

    @Override
    public int getMessageType() {
        return 11;
    }
}
