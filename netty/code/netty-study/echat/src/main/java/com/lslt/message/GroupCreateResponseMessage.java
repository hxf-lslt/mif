package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 0:46
 */
public class GroupCreateResponseMessage extends AbstractResponseMessage{


    public GroupCreateResponseMessage(boolean status, String info) {
        super(status,info);
    }

    @Override
    public int getMessageType() {
        return 5;
    }


}
