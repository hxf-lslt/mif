package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:42
 */
public class GroupQuitResponseMessage extends AbstractResponseMessage{


    public GroupQuitResponseMessage(boolean status, String info) {
        super(status, info);
    }

    @Override
    public int getMessageType() {
        return 13;
    }
}
