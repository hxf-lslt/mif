package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/11 22:23
 */
public class ChatRequestMessage extends AbstractMessage{

    /**
     * 来源
     */
    private String from;
    /**
     * 目标
     */
    private String to;
    /**
     * 内容
     */
    private String content;


    public ChatRequestMessage(String from, String to, String content) {
        this.from = from;
        this.to = to;
        this.content = content;
    }

    @Override
    public int getMessageType() {
        return 2;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
