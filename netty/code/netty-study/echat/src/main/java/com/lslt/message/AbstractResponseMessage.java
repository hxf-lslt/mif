package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 23:19
 */
public abstract class AbstractResponseMessage extends AbstractMessage {

    protected boolean status;
    protected String info;

    public AbstractResponseMessage() {
    }

    public AbstractResponseMessage(boolean status, String info) {
        this.status = status;
        this.info = info;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "AbstractResponseMessage{" +
                "status=" + status +
                ", info='" + info + '\'' +
                '}';
    }
}
