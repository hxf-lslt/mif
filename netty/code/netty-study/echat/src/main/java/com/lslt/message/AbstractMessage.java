package com.lslt.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lslt
 * @description
 * @date 2024/5/9 22:04
 */
public abstract class AbstractMessage implements Serializable {


    /**
     * 获取消息类型
     * @return 消息类型
     */
    public abstract int getMessageType();

    public int getSequenceId(){
        return 12345;
    }
    public static Map<Integer,Class<? extends AbstractMessage>> messageType = new HashMap();
    static {
        messageType.put(0,LoginRequestMessage.class);
        messageType.put(1,LoginResponseMessage.class);
        messageType.put(2,ChatRequestMessage.class);
        messageType.put(3,ChatResponseMessage.class);
        messageType.put(4,GroupCreateRequestMessage.class);
        messageType.put(5,GroupCreateResponseMessage.class);
        messageType.put(6,GroupChatRequestMessage.class);
        messageType.put(7,GroupChatResponseMessage.class);
        messageType.put(8,GroupMembersRequestMessage.class);
        messageType.put(9,GroupMembersResponseMessage.class);
        messageType.put(10,GroupJoinRequestMessage.class);
        messageType.put(11,GroupJoinResponseMessage.class);
        messageType.put(12,GroupQuitRequestMessage.class);
        messageType.put(13,GroupQuitResponseMessage.class);
    }
}
