package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:09
 */
public class GroupChatResponseMessage extends AbstractResponseMessage{


    private String from;

    private String content;

    public GroupChatResponseMessage(String from, String content) {
        this.from = from;
        this.content = content;
    }

    public GroupChatResponseMessage(boolean status, String info) {
        super(status, info);
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int getMessageType() {
        return 7;
    }

    @Override
    public String toString() {
        return "GroupChatResponseMessage{" +
                "from='" + from + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
