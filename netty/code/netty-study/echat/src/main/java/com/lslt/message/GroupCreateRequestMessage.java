package com.lslt.message;

import java.util.Set;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 0:44
 */
public class GroupCreateRequestMessage extends AbstractMessage{

    /**
     * 群名
     */
    private String groupName;

    /**
     * 成员
     */
    private Set<String> members;


    public GroupCreateRequestMessage(String groupName) {
        this.groupName = groupName;
    }

    public GroupCreateRequestMessage(String groupName, Set<String> members) {
        this.groupName = groupName;
        this.members = members;
    }

    @Override
    public int getMessageType() {
        return 4;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Set<String> getMembers() {
        return members;
    }

    public void setMembers(Set<String> members) {
        this.members = members;
    }
}
