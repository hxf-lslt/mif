package com.lslt.server.handler;

import com.lslt.message.GroupMembersRequestMessage;
import com.lslt.message.GroupMembersResponseMessage;
import com.lslt.server.session.GroupSession;
import com.lslt.server.session.GroupSessionFactory;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Set;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:33
 */
public class GroupMembersHandler extends SimpleChannelInboundHandler<GroupMembersRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupMembersRequestMessage message) throws Exception {
        GroupSession groupSession = GroupSessionFactory.getGroupSession();
        if(groupSession.isGroupExist(message.getGroupName())){
            Set<String> members = groupSession.getMembers(message.getGroupName());
             ctx.channel().writeAndFlush(new GroupMembersResponseMessage(members.toString()));
        }else {
            ctx.channel().writeAndFlush(new GroupMembersResponseMessage(false,"群名不存在"));
        }

    }
}
