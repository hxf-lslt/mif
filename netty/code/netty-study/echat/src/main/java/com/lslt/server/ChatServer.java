package com.lslt.server;

import com.lslt.codec.MessageCodec;
import com.lslt.codec.ProtocolFrameDecoder;
import com.lslt.server.handler.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LoggingHandler;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:39
 */
public class ChatServer {

    public static void main(String[] args) {
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        ChannelFuture future = new ServerBootstrap()
                .group(boss, worker)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel channel) throws Exception {
                        channel.pipeline().addLast(new LoggingHandler());
                        channel.pipeline().addLast(new ProtocolFrameDecoder());
                        channel.pipeline().addLast(new MessageCodec());
                        channel.pipeline().addLast(new LoginRequestHandler());
                        channel.pipeline().addLast(new ChatRequestHandler());
                        channel.pipeline().addLast(new GroupCreateHandler());
                        channel.pipeline().addLast(new GroupChatHandler());
                        channel.pipeline().addLast(new GroupMembersHandler());
                        channel.pipeline().addLast(new GroupJoinHandler());
                        channel.pipeline().addLast(new GroupQuitHandler());
                        channel.pipeline().addLast(new QuitHandler());
                    }
                }).bind(8080);

        future.channel().closeFuture().addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                boss.shutdownGracefully();
                worker.shutdownGracefully();
            }
        });
    }
}
