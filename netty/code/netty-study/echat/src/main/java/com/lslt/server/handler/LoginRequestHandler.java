package com.lslt.server.handler;

import com.lslt.message.LoginRequestMessage;
import com.lslt.message.LoginResponseMessage;
import com.lslt.server.service.UserServiceFactory;
import com.lslt.server.session.SessionFactory;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lslt
 * @description
 * @date 2024/5/11 22:38
 */
public class LoginRequestHandler extends SimpleChannelInboundHandler<LoginRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginRequestMessage loginRequestMessage) throws Exception {
        //收到用户登录信息
        String username = loginRequestMessage.getUsername();
        String password = loginRequestMessage.getPassword();
        boolean result = UserServiceFactory.getUserService().login(username, password);
        LoginResponseMessage message;
        if(result){
            //放到会话中
            SessionFactory.getSession().bind(ctx.channel(),username);
            message = new LoginResponseMessage(true,"登录成功");
        }else {
            message = new LoginResponseMessage(false,"用户名或密码错误");
        }
        ctx.channel().writeAndFlush(message);
    }
}
