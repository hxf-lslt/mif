package com.lslt.client;

import com.lslt.codec.MessageCodec;
import com.lslt.codec.ProtocolFrameDecoder;
import com.lslt.message.*;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:43
 */

@Slf4j
public class ChatClient {

    private static final ExecutorService executorService = Executors.newFixedThreadPool(2);

    private static void processTask(Runnable task) {
        executorService.execute(task);
    }

    public static void main(String[] args) {
        NioEventLoopGroup worker = new NioEventLoopGroup();
        CountDownLatch WAIT_FOR_LOGIN = new CountDownLatch(1);
        AtomicBoolean loginStatus = new AtomicBoolean(false);
        try {
            ChannelFuture future = new Bootstrap()
                    .group(worker)
                    .channel(NioSocketChannel.class)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel channel) throws Exception {
//                            channel.pipeline().addLast(new LoggingHandler());
                            channel.pipeline().addLast(new ProtocolFrameDecoder());
                            channel.pipeline().addLast(new MessageCodec());

                            channel.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                                @Override
                                public void channelRead(ChannelHandlerContext ctx, Object msg) {
                                    log.info("收到消息：{}", msg);

                                    if (msg instanceof LoginResponseMessage) {
                                        LoginResponseMessage response = (LoginResponseMessage) msg;
                                        if (response.isStatus()) {
                                            loginStatus.set(true);
                                        }
                                        WAIT_FOR_LOGIN.countDown();
                                    }
                                }

                                @Override
                                public void channelActive(ChannelHandlerContext ctx) {
//                                ctx.channel().writeAndFlush(new LoginRequestMessage());
                                    //建立连接，可以登录
                                    processTask(() -> {
                                        Scanner scanner = new Scanner(System.in);
                                        System.out.println("请输入用户名：");
                                        String username = scanner.nextLine();
                                        System.out.println("请输入密码：");
                                        String password = scanner.nextLine();
                                        LoginRequestMessage message = new LoginRequestMessage(username, password);
                                        ctx.channel().writeAndFlush(message);

                                        //等待服务器返回的登录结果
                                        try {
                                            WAIT_FOR_LOGIN.await();
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        if (!loginStatus.get()) {
                                            //登录失败直接退出
                                            ctx.channel().close();
                                            return;
                                        }
//                                        boolean isContinue = true;
                                        while (true) {
                                            System.out.println("==================================");
                                            System.out.println("send [username] [content]");
                                            System.out.println("gsend [group name] [content]");
                                            System.out.println("gcreate [group name] [m1,m2,m3...]");
                                            System.out.println("gmembers [group name]");
                                            System.out.println("gjoin [group name]");
                                            System.out.println("gquit [group name]");
                                            System.out.println("quit");
                                            System.out.println("==================================");
                                            String[] command = scanner.nextLine().split(" ");
                                            switch (command[0]){
                                                case "send":
                                                    ChatRequestMessage chatRequestMessage = new ChatRequestMessage(username, command[1], command[2]);
                                                    ctx.channel().writeAndFlush(chatRequestMessage);
                                                    break;
                                                case "gsend":
                                                    GroupChatRequestMessage groupChatRequestMessage = new GroupChatRequestMessage(username, command[1], command[2]);
                                                    ctx.channel().writeAndFlush(groupChatRequestMessage);
                                                    break;
                                                case "gcreate":
                                                    HashSet<String> members = new HashSet<>(Arrays.asList(command[2].split(",")));
                                                    members.add(username);
                                                    GroupCreateRequestMessage groupCreateRequestMessage = new GroupCreateRequestMessage(command[1], members);
                                                    ctx.channel().writeAndFlush(groupCreateRequestMessage);
                                                    break;
                                                case "gmembers":
                                                    GroupMembersRequestMessage groupMembersRequestMessage = new GroupMembersRequestMessage(command[1]);
                                                    ctx.channel().writeAndFlush(groupMembersRequestMessage);
                                                    break;
                                                case "gjoin":
                                                    GroupJoinRequestMessage groupJoinRequestMessage = new GroupJoinRequestMessage(command[1],username);
                                                    ctx.channel().writeAndFlush(groupJoinRequestMessage);
                                                    break;
                                                case "gquit":
                                                    GroupQuitRequestMessage groupQuitRequestMessage = new GroupQuitRequestMessage(command[1],username);
                                                    ctx.channel().writeAndFlush(groupQuitRequestMessage);
                                                    break;
                                                case "quit":
                                                    ctx.channel().close();
                                                    return;
                                                default:
                                                    break;
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }).connect(new InetSocketAddress("localhost", 8080)).sync();
            future.channel().closeFuture().addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture future) throws Exception {
                    executorService.shutdown();
                    worker.shutdownGracefully();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
