package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:41
 */
public class GroupQuitRequestMessage extends AbstractMessage {


    private String groupName;
    private String from;

    public GroupQuitRequestMessage(String groupName, String from) {
        this.groupName = groupName;
        this.from = from;
    }

    @Override
    public int getMessageType() {
        return 12;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
