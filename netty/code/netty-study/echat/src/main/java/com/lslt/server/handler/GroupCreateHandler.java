package com.lslt.server.handler;

import com.lslt.message.GroupCreateRequestMessage;
import com.lslt.message.GroupCreateResponseMessage;
import com.lslt.server.session.Group;
import com.lslt.server.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 0:49
 */
public class GroupCreateHandler extends SimpleChannelInboundHandler<GroupCreateRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupCreateRequestMessage message) throws Exception {
        String groupName = message.getGroupName();

        Group group = GroupSessionFactory.getGroupSession().createGroup(groupName, message.getMembers());

        if (group == null) {
            ctx.channel().writeAndFlush(new GroupCreateResponseMessage(true, "[" + groupName + "]创建成功"));
            List<Channel> membersChannel = GroupSessionFactory.getGroupSession().getMembersChannel(groupName);
            for (Channel channel : membersChannel) {
                channel.writeAndFlush(new GroupCreateResponseMessage(true, "您已被拉入[" + groupName + "]群聊"));
            }
        } else {
            ctx.channel().writeAndFlush(new GroupCreateResponseMessage(false, "[" + groupName + "]创建失败"));
        }
    }
}
