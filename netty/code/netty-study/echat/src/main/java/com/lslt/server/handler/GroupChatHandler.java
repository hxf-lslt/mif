package com.lslt.server.handler;

import com.lslt.message.GroupChatRequestMessage;
import com.lslt.message.GroupChatResponseMessage;
import com.lslt.server.session.GroupSession;
import com.lslt.server.session.GroupSessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:13
 */
public class GroupChatHandler extends SimpleChannelInboundHandler<GroupChatRequestMessage> {


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupChatRequestMessage message) throws Exception {
        GroupSession groupSession = GroupSessionFactory.getGroupSession();
        if (groupSession.isGroupExist(message.getGroupName())) {
            List<Channel> membersChannel = groupSession.getMembersChannel(message.getGroupName());
            for (Channel channel : membersChannel) {
                channel.writeAndFlush(new GroupChatResponseMessage(message.getFrom(),message.getContent()));
            }
        }else {
            ctx.channel().writeAndFlush(new GroupChatResponseMessage(false,"群聊不存在"));
        }
    }
}
