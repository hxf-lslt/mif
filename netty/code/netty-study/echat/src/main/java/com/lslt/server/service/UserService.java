package com.lslt.server.service;

/**
 * @author lslt
 * @description 用户管理接口
 * @date 2024/5/10 22:13
 */
public interface UserService {
    /**
     * 登录
     * @param username 用户名
     * @param password 密码
     * @return 登录成功返回 true, 否则返回 false
     */
    boolean login(String username, String password);
}
