package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/12 1:28
 */
public class GroupMembersRequestMessage extends AbstractMessage{


    /**
     * 群名
     */
    private String groupName;


    public GroupMembersRequestMessage(String groupName){
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public int getMessageType() {
        return 8;
    }
}
