package com.lslt.codec;

import com.lslt.message.AbstractMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author lslt
 * @description 消息编解码器
 * @date 2024/5/9 22:07
 * <p>
 * 自定义协议要素
 * 1.魔数，用来判断消息是否有效
 * 2.版本号，支持协议升级
 * 3.序列化算法，json,protob,hessian,jdk
 * 4.指令类型，和业务相关
 * 5.请求序号，为双工通信，提供异步能力
 * 6.正文长度
 * 7正文内容
 */
@Slf4j
public class MessageCodec extends ByteToMessageCodec<AbstractMessage> {

    @Override
    public void encode(ChannelHandlerContext ctx, AbstractMessage msg, ByteBuf out) throws Exception {

        //1.魔数
        out.writeBytes(new byte[]{'L', 'S', 'L', 'T'});
        //2.版本号
        out.writeByte(1);
        //3.序列化算法 0-jdk
        out.writeByte(0);
        //4.指令类型
        out.writeByte(msg.getMessageType());
        //5.请求序列号
        out.writeInt(msg.getSequenceId());
        //对齐
        out.writeByte(0xff);
        //将对象输出成byte数组
        byte[] bytes = Serializer.Algorithm.JSON.serialize(msg);
        //6.写入正文长度
        out.writeInt(bytes.length);
        //7.写入正文
        out.writeBytes(bytes);
    }

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

        //魔数
        byte[] magicNum = new byte[4];
        in.readBytes(magicNum);
        //版本号
        byte version = in.readByte();
        //序列化类型
        byte serializableType = in.readByte();
        //指令类型
        byte messageType = in.readByte();
        //请求序列号
        int sequenceId = in.readInt();
        //对齐
        in.readByte();
        //长度
        int length = in.readInt();
        byte[] bytes = new byte[length];
        in.readBytes(bytes);
        Class<? extends AbstractMessage> messageClass = AbstractMessage.messageType.get((int)messageType);
        AbstractMessage message = Serializer.Algorithm.JSON.deserialize(messageClass, bytes);
        out.add(message);

//        log.info("魔数：{},版本号：{},序列化类型：{},指令类型：{},请求序列号：{},长度：{},内容：{}"
//                , new String(magicNum), version, serializableType, messageType, sequenceId, length, message);
    }
}
