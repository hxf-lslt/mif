package com.lslt.message;

/**
 * @author lslt
 * @description
 * @date 2024/5/11 22:23
 */
public class ChatResponseMessage extends AbstractResponseMessage{

    /**
     * 来源
     */
    private String from;

    /**
     * 内容
     */
    private String content;

    public ChatResponseMessage(){

    }

    public ChatResponseMessage(String from, String content) {
        this(true,"发送成功");
        this.from = from;
        this.content = content;
    }

    public ChatResponseMessage(boolean status, String info) {
        super(status, info);
    }

    @Override
    public int getMessageType() {
        return 2;
    }


    @Override
    public String toString() {
        return "ChatResponseMessage{" +
                "status=" + status +
                ", info='" + info + '\'' +
                ", from='" + from + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
