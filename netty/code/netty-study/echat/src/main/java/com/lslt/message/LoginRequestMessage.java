package com.lslt.message;

/**
 * @author lslt
 * @description 登录请求消息
 * @date 2024/5/9 22:37
 */
public class LoginRequestMessage extends AbstractMessage {

    private String username;
    private String password;


    public LoginRequestMessage(){}


    public LoginRequestMessage(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public int getMessageType() {
        return 0;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginRequestMessage{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
