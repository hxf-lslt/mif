package com.lslt.server.session;

/**
 * @author lslt
 * @description
 * @date 2024/5/10 22:19
 */
public abstract class SessionFactory {

    private static Session session = new SessionMemoryImpl();

    public static Session getSession() {
        return session;
    }
}