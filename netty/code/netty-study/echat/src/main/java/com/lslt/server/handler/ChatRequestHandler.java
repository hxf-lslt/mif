package com.lslt.server.handler;

import com.lslt.message.ChatRequestMessage;
import com.lslt.message.ChatResponseMessage;
import com.lslt.server.session.SessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lslt
 * @description
 * @date 2024/5/11 22:38
 */
public class ChatRequestHandler extends SimpleChannelInboundHandler<ChatRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ChatRequestMessage message) throws Exception {
        String to = message.getTo();
        Channel channel = SessionFactory.getSession().getChannel(to);

        //判断是否在线
        if(null != channel){
            channel.writeAndFlush(new ChatResponseMessage(message.getFrom(),message.getContent()));
        }else{
            ctx.channel().writeAndFlush(new ChatResponseMessage(false,"对方不在线"));
        }
    }
}
