# NIO基础



**Java NIO：**（New I/O）/（Non-blocking IO）是Java 1.4引入的一种新的I/O模型，它提供了更高效、更灵活的I/O操作方式，相比传统的I/O模型，Java NIO更适用于需要处理大量连接的网络应用，比如网络服务器。

## 三大组件

- **Channel（通道）**：类似于流，但Channel是双向的，既可以读也可以写。主要的Channel实现有`FileChannel`（用于文件操作）、`SocketChannel`（客户端TCP连接）、`ServerSocketChannel`（服务器端TCP连接）和`DatagramChannel`（UDP通信）。
- **Buffer（缓冲区）**：数据容器，用于存储不同数据类型的数据。主要的Buffer类型有`ByteBuffer`、`CharBuffer`、`IntBuffer`等，所有缓冲区都是`Buffer`类的子类。
- **Selector（选择器）**：允许单线程处理多个通道的注册和监听事件，从而实现高效的I/O多路复用。

![NIO-模型](NIO.assets/NIO-模型.svg)



## **特点**

- **非阻塞I/O：**Java NIO支持非阻塞I/O操作，一个线程可以同时处理多个通道，避免了传统I/O模型中一个连接阻塞导致其他连接无法处理的问题。
- **内存管理：**Java NIO通过缓冲区管理数据，可以更灵活地控制内存的使用。
- **多路复用：**选择器可以同时监听多个通道的事件，减少了线程数量和系统资源的消耗。



## Buffer的基本使用

1.先简单的使用一下`ByteBuffer`

> **目标：**通过FileChannel通道，ByteBuffer读取文件数据。（详细代码见：code/netty/nio-test/TestByteBuffer）

```java
public static void main(String[] args) {
    //使用 FileChannel 读取文件
    try (FileChannel channel = new FileInputStream("data.txt").getChannel()) {
        //将文件通过 Channel 读取到 ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(10);
        int len ;
        while ((len = channel.read(buffer)) > 0){

            log.debug("读取字节数：{}",len);
            buffer.flip(); //切换模式
            while(buffer.hasRemaining()){
                //读取一个字节
                byte b = buffer.get();
                log.debug("读取的字符是：{}",(char)b);
            }
            buffer.clear(); //清除读位置
        }

    } catch (IOException e) {
        e.printStackTrace();
    }
}
```

### Buffer内部结构读写转换

![Buffer-read-write-change](NIO.assets/Buffer-read-write-change.svg)

> `get()`方法会让position的位置向后移动，如果想要重复的读取数据，可以：
>
> - 调用rewind(),将position位置重新置为0
> - 调用get(int i),将会获取索引i中的内容，同时也不会移动position的位置



## 网络编程

1. 单线程-阻塞模式

>  服务端：

```java
@Slf4j
public class Server {


    public static void main(String[] args) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(16);
        //创建服务器Channel
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //绑定端口
        ssc.bind(new InetSocketAddress(8080));

        List<SocketChannel> channels = new ArrayList<>();

        while(true){
            //等待客户端的连接
            log.info("waiting connected....");
            SocketChannel sc = ssc.accept();  //阻塞方法
            log.info("connected: {}",sc);
            channels.add(sc);

            //处理每条 Channel 中的消息
            for (SocketChannel channel : channels) {
                //接受读取消息
                log.info("before read....{}",channel);
                channel.read(buffer); //阻塞。线程不能往下执行
                buffer.flip();
                byte[] buf = new byte[buffer.limit()];
                buffer.get(buf,0,buffer.limit());
                log.debug("接收到的消息：{}",new String(buf));
                buffer.clear();
                log.info("after read....{}",channel);
            }
        }
    }
}
```

> 客户端：

```java
public class Client {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.connect(new InetSocketAddress("localhost",8080));
        System.out.println(sc);
    }
}
```

代码流程详解：

![NIO-网络编程-单线程-阻塞模式](NIO.assets/NIO-网络编程-单线程-阻塞模式.svg)

2. 单线程-非阻塞模式

> 服务端：ServerSocket设置为非阻塞的configureBlocking(false)，则在accept方法处就不会阻塞，同时Socket设置为非阻塞，则在read()方法处就不会阻塞

```java
 public static void main(String[] args) throws IOException {

        ByteBuffer buffer = ByteBuffer.allocate(16);
        //创建服务器Channel
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //设置非阻塞
        ssc.configureBlocking(false);
        //绑定端口
        ssc.bind(new InetSocketAddress(8080));
        List<SocketChannel> channels = new ArrayList<>();
        while (true) {
            SocketChannel sc = ssc.accept();  //阻塞方法
            if (null != sc) {
                log.info("connected: {}", sc);
                sc.configureBlocking(false); //设置为非阻塞模式
                channels.add(sc);
            }
            //处理每条 Channel 中的消息
            for (SocketChannel channel : channels) {
                int read = channel.read(buffer);//阻塞。线程不能往下执行
                if(read > 0){
                    buffer.flip();
                    byte[] buf = new byte[buffer.limit()];
                    buffer.get(buf, 0, buffer.limit());
                    log.debug("接收到的消息：{}", new String(buf));
                    buffer.clear();
                    log.info("after read....{}", channel);
                }
            }
        }
    }
```

3. 使用Selector-非阻塞模式

> 服务端：

```java
public static void nonBlocking() throws IOException {
        //创建服务器Channel
        ServerSocketChannel ssc = ServerSocketChannel.open();
        //设置非阻塞
        ssc.configureBlocking(false);
        //绑定端口
        ssc.bind(new InetSocketAddress(8080));
        //建立selector
        Selector selector = Selector.open();
        //将channel注册到selector中
        SelectionKey sscKey = ssc.register(selector, 0, null);
        //关注感兴趣的事件（accept事件）
        sscKey.interestOps(SelectionKey.OP_ACCEPT);
        log.info("注册的事件：{}", sscKey);
        while (true) {
            //selector 监听事件 阻塞，等待事件的发生
            //如果有事件未处理时 则不会阻塞
            //事件要么被处理，要么取消。  key.cancel();
            selector.select();
            //拿到selector中的所有事件
            Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                //如果是连接事件，
                if (key.isAcceptable()) {
                    log.info("触发事件的key:{}", key);
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    //上面只是注册了accept事件(客户端发生连接)
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    SelectionKey scKey = sc.register(selector, 0, null);
                    //关注读事件
                    scKey.interestOps(SelectionKey.OP_READ);
                    log.info("连接的客户端：{}", sc);
                } else if (key.isReadable()) {
                    SocketChannel channel = (SocketChannel) key.channel();
                    ByteBuffer buffer = ByteBuffer.allocate(16);
                    channel.read(buffer);
                    buffer.flip();
                    byte[] buf = new byte[buffer.limit()];
                    buffer.get(buf, 0, buffer.limit());
                    log.debug("接收到的消息：{}", new String(buf));
                    buffer.clear();
                }
                iter.remove(); //selector 处理完事件后，不会从事件集合中移除。需要自己手动删除。
            }
        }
    }

```

Selector模式代码详解图：

![NIO-网络编程-selector](NIO.assets/NIO-网络编程-selector.svg)

> 上面的代码，有个问题：
>
> 1. 如果客户端直接关闭，服务端会抛出一个IOException
> 2. 如果客户端调用close()关闭，则会一直循环的调用read事件。
>
> 鉴于上面出现的问题的原因是：不管客户端是正常关闭还是一样关闭，都会调用一次read事件。
>
> 对于事件，不是处理就是要取消。因此修改代码：

```java
else if (key.isReadable()) {
    try {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(16);
        int read = channel.read(buffer);
        //说明客户端关闭连接
        if(read == -1){
            key.cancel();
        }
        buffer.flip();
        byte[] buf = new byte[buffer.limit()];
        buffer.get(buf, 0, buffer.limit());
        log.debug("接收到的消息：{}", new String(buf));
        buffer.clear();
    } catch (IOException e) {
        e.printStackTrace();
        //异常关闭时
        key.cancel();
    }

}
```



> 消息边界问题：假设`ByteBuffer buffer = ByteBuffer.allocate(4);`就分配4个字节的空间，如果客户端发送`中国`字符串(UTF-8),就会出现乱码的问题。

![image-20240502234808199](NIO.assets/image-20240502234808199.png)

所以客户端和服务端需要按照一定的协议来处理消息。比如说按`\n`作为结束符，Buffer容量不够扩容，TLV协议那种等。

**修改上面遇到的消息边界问题：**

每个channel维护自己的Buffer

```java
while (iter.hasNext()) {
    SelectionKey key = iter.next();
    if (key.isAcceptable()) {
        ...
        ByteBuffer buffer = ByteBuffer.allocate(16);
        //每一个channel绑定一个Buffer
        SelectionKey scKey = sc.register(selector, 0, buffer);
        ...
    } else if (key.isReadable()) {
        SocketChannel channel = (SocketChannel) key.channel();
        //拿到附件。
        ByteBuffer buffer = (ByteBuffer)key.attachment();
        int read = channel.read(buffer);
        //说明客户端关闭连接
        if(read == -1){
            key.cancel();
        }
        split(buffer); //调用的找边界的方法
        //当position的位置和limit的位置一样时，说明没有读取Buffer内容（没有遇到分割符），需要扩容，容量为以前的两倍。
        if(buffer.position() == buffer.limit()){
            ByteBuffer newBuffer = ByteBuffer.allocate(buffer.capacity() * 2);
            buffer.flip();
            newBuffer.put(buffer);
            key.attach(newBuffer);
            buffer = null;
        }
        
        ...

    }
  
}
```

4. 使用多线程配合Selector

> 服务端：

```java
@Slf4j
public class MultiThreadServer {

    public static void main(String[] args) throws IOException {

        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.bind(new InetSocketAddress(8080));
        ssc.configureBlocking(false);
        Thread.currentThread().setName("boss");

        Selector boss = Selector.open();
        SelectionKey bossKey = ssc.register(boss, 0, null);
        bossKey.interestOps(SelectionKey.OP_ACCEPT);

        Worker worker = new Worker("worker-01");
        while (true){
            boss.select();
            Iterator<SelectionKey> iter = boss.selectedKeys().iterator();
            while (iter.hasNext()){
                SelectionKey key = iter.next();
                iter.remove();
                if(key.isAcceptable()){
                    ServerSocketChannel channel = (ServerSocketChannel)key.channel();
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    log.info("注册客户信息：{}",sc);
                    worker.register(sc);
                }
            }
        }
    }

    static class Worker implements Runnable{

        private Thread thread;
        private Selector selector;
        private String name;

        private ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

        private volatile boolean started = false;

        public Worker(String name){
            this.name = name;
        }


        public void register(SocketChannel sc) throws IOException {
            if(!started){
                this.selector = Selector.open();
                this.thread = new Thread(this,this.name);
                this.thread.start();
                this.started = true;
            }

            queue.add(()->{
                try {
                    sc.register(this.selector, SelectionKey.OP_READ, null);
                } catch (ClosedChannelException e) {
                    e.printStackTrace();
                }
            });
            selector.wakeup();
        }

        @Override
        public void run() {
            while (true){
                try {
                    selector.select();
                    Runnable task = queue.poll();
                    if(null != task){
                        task.run();
                    }
                    Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
                    while (iter.hasNext()){
                        SelectionKey key = iter.next();
                        iter.remove();
                        if (key.isReadable()) {
                            ByteBuffer buffer = ByteBuffer.allocate(16);
                            SocketChannel channel = (SocketChannel)key.channel();
                            channel.read(buffer);
                            buffer.flip();
                            String str = StandardCharsets.UTF_8.decode(buffer).toString();
                            log.info("接收到客户端消息：{},channel:{}",str,channel);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
```

![NIO-多线程模式](NIO.assets/NIO-多线程模式.svg)
