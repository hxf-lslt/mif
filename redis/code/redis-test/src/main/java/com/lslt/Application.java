package com.lslt;

import redis.clients.jedis.JedisPooled;

/**
 * @author lslt
 * @description
 * @date 2024/4/15 13:01
 */
public class Application {


    public static void main(String[] args) {

//        JedisPool pool = new JedisPool("192.168.0.129",6379);
//
//        try(Jedis jedis = pool.getResource()) {
//            String status = jedis.set("greet", "hello");
//            System.out.println(status);
//            System.out.println(jedis.get("greet"));
//        }

//        hashType();
//        listType();
//        setType();
        sortedSetType();
    }

    public static void sortedSetType(){
        JedisPooled jedis = new JedisPooled("192.168.0.129", 6379);

        //添加成员到集合中
//        long zadd = jedis.zadd("leaderboard", 400, "player4");
//        System.out.println(zadd); //1

        //获取指定范围成员
//        List<String> leaderboard = jedis.zrange("leaderboard", 0, -1);
//        leaderboard.forEach(System.out::println);
//        System.out.println("////////////////");
//        List<String> leaderboard1 = jedis.zrange("leaderboard", ZRangeParams.zrangeByScoreParams(200, 400));
//        leaderboard1.forEach(System.out::println);

        //获取分数范围的集合成员
//        List<String> leaderboard = jedis.zrangeByScore("leaderboard", 100, 500, 0, 2);
//        leaderboard.forEach(System.out::println);

        //获取集合成员分数
//        Double zscore = jedis.zscore("leaderboard", "player4");
//        System.out.println(zscore); //400.0
//        Double zscore1 = jedis.zscore("leaderboard", "player6");
//        System.out.println(zscore1);//null

        //获取成员排名
//        Long zrank = jedis.zrank("leaderboard", "player3");
//        System.out.println(zrank); //2
//        Long zrank1 = jedis.zrank("leaderboard", "player0");
//        System.out.println(zrank1); //null
//        Long zrevrank = jedis.zrevrank("leaderboard", "player3");
//        System.out.println(zrevrank); //1

        //获取集合数量
//        long leaderboard = jedis.zcard("leaderboard");
//        System.out.println(leaderboard);//4

//        //增加成员分数
//        double zincrby = jedis.zincrby("leaderboard", 45, "player2");
//        System.out.println(zincrby); //245.0

        //移除指定成员
//        long zrem = jedis.zrem("leaderboard", "player2","player3");
//        System.out.println(zrem); //2

        //移除分数范围分成员
        long leaderboard = jedis.zremrangeByScore("leaderboard", 0, 100);
        System.out.println(leaderboard); //0
    }


    public static void setType(){

        JedisPooled jedis = new JedisPooled("192.168.0.129", 6379);

        //添加元素
//        long sadd = jedis.sadd("phoneBrand", "Apple", "华为", "vivo");
//        System.out.println(sadd);

        //获取集合所有元素
//        Set<String> phoneBrand = jedis.smembers("phoneBrand");
//        Set<String> phoneBrand1 = jedis.smembers("phoneBrand1");
//        phoneBrand.forEach(System.out::println);
//        phoneBrand1.forEach(System.out::println);

        //判断是否存在成员
//        boolean sismember = jedis.sismember("phoneBrand", "Apple");
//        System.out.println(sismember); //true
//        boolean sismember1 = jedis.sismember("phoneBrand1", "Apple");
//        System.out.println(sismember1); //false

        //获取集合数量
//        long phoneBrand = jedis.scard("phoneBrand");
//        long phoneBrand1 = jedis.scard("phoneBrand1");
//        System.out.println(phoneBrand); //3
//        System.out.println(phoneBrand1); //0

        //移除指定元素
//        long srem = jedis.srem("phoneBrand", "Apple");
//        System.out.println(srem); //1
//        long srem1 = jedis.srem("phoneBrand1", "Apple");
//        System.out.println(srem1); //0

        //随机移除元素并返回
//        String phoneBrand = jedis.spop("phoneBrand");
//        System.out.println(phoneBrand); //vivo

        //交集 并集 差集
        // phoneBrand ["Apple", "华为", "vivo"]
//        long sadd = jedis.sadd("phoneBrand", "Apple", "华为", "vivo");
////        long sadd = jedis.sadd("phoneBrand", "apple", "华为", "vivo","Samsung");
//        Set<String> sinter = jedis.sinter("phoneBrand1", "phoneBrand");
//        Set<String> sunion = jedis.sunion("phoneBrand1", "phoneBrand");
//        Set<String> sdiff = jedis.sdiff("phoneBrand1", "phoneBrand");
//
//        System.out.println("sinter:"+ sinter.toString());
//        System.out.println("sunion:"+ sunion.toString());
//        System.out.println("sdiff:"+ sdiff.toString());

        //从一个集合移动元素到另一个集合
        long smove = jedis.smove("phoneBrand1", "phoneBrand", "vivo");
        System.out.println(smove); //1

        long smove1 = jedis.smove("phoneBrand1", "phoneBrand", "apple");
        System.out.println(smove1);//1

    }


    public static void listType(){
        JedisPooled jedis = new JedisPooled("192.168.0.129", 6379);
        //左插和右插
//        long lpush = jedis.lpush("mylist", "hello", "world");
//        System.out.println(lpush);
//        long rpush = jedis.rpush("mylist", "redis", "hello", "!!!");
//        System.out.println(rpush);

        //返回指定范围元素
//        List<String> mylist = jedis.lrange("mylist", 1, 4);
//        mylist.forEach(System.out::println);

        //获取指定位置元素
//        String mylist = jedis.lindex("mylist", 4);
//        System.out.println(mylist);

        //获取列表长度
//        long mylist = jedis.llen("mylist");
//        System.out.println(mylist);

        //删除并返回左右侧元素
//        String lvalue = jedis.lpop("mylist");
//        String rvalue = jedis.rpop("mylist");
//        System.out.println(lvalue); //world
//        System.out.println(rvalue); //!!!
//        long mylist = jedis.llen("mylist");
//        System.out.println(mylist); //3

        //设置列表指定位置元素
//        String lset = jedis.lset("mylist", 0, "who");
//        System.out.println(lset); //OK
//
//        long linsert1 = jedis.linsert("mylist", ListPosition.BEFORE, "00", "hello");
//        long linsert2 = jedis.linsert("mylist", ListPosition.AFTER, "who", "hello");
//        System.out.println(linsert1); //-1
//        System.out.println(linsert2); //4

        //移除指定元素
        long lrem = jedis.lrem("mylist", 0, "hello");
        System.out.println(lrem); //2
    }

    public static void hashType() {
        JedisPooled jedis = new JedisPooled("192.168.0.129", 6379);

        //1.添加映射关系
//        long hset = jedis.hset("user:id:112", "username", "lisi");
//        System.out.println(hset);
//        long hset1 = jedis.hset("user:id:112", "age", "13");
//        System.out.println(hset1);

        //获取字段映射关系
//        String username = jedis.hget("user:id:112", "username");
//        String age = jedis.hget("user:id:112", "age");
//        String name = jedis.hget("user:id:112", "name");
//        System.out.println(username); //lisi
//        System.out.println(age); //13
//        System.out.println(name); //null

        //获取所有字段与值的映射关系
//        Map<String, String> user = jedis.hgetAll("user:id:112");
//        user.forEach((k, v) -> {
//            System.out.println("field:" + k + "\tvalue:" + v);
//        });

        //获取所有字段
//        Set<String> userFields = jedis.hkeys("user:id:112");
//        userFields.forEach(System.out::println);

        //获取所有字段值
//        List<String> userFieldValues = jedis.hvals("user:id:112");
//        userFieldValues.forEach(System.out::println);

        //判断field是否存在
//        boolean username = jedis.hexists("user:id:112", "username");
//        System.out.println(username);
//        boolean name = jedis.hexists("user:id:112", "name");
//        System.out.println(name);

        //删除指定的filed
//        long hdel = jedis.hdel("user:id:112", "username", "name");
//        System.out.println(hdel);

        //查询field数量
//        long hlen = jedis.hlen("user:id:112");
//        System.out.println(hlen);

        //增加整数字段值
//        long age = jedis.hincrBy("user:id:112", "age", 20);
//        System.out.println(age); //33

        //增加浮点数字段值
        double salary = jedis.hincrByFloat("user:id:111", "salary", -100);
        System.out.println(salary);
    }


    public static void stringType() {
        JedisPooled jedis = new JedisPooled("192.168.0.129", 6379);
//        String value = jedis.get("greet");
//        System.out.println(value);
//
//        String setexStatus = jedis.setex("name", 10, "lisi");
//        System.out.println(setexStatus);

//        long append = jedis.append("greet", "world");
//        System.out.println(append);
//        String greet = jedis.getrange("greet", 0, 4);
//        System.out.println(greet);
//        long setrange = jedis.setrange("name", 3, "_hello_");
//        System.out.println(jedis.get("name"));
//        System.out.println(setrange);
//        long greet = jedis.strlen("greet");
//        System.out.println(greet);
//        System.out.println(jedis.get("age"));
//        long age = jedis.incr("age");
//        System.out.println(age);

//        System.out.println(jedis.get("age"));
//        long age = jedis.decr("age");
//        System.out.println(age);
//        System.out.println(jedis.get("age"));
//        long age = jedis.incrBy("age",10);
//        System.out.println(age);

        System.out.println(jedis.get("age"));
        long age = jedis.decrBy("age", 10);
        System.out.println(age);
    }
}
