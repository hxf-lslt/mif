# Redis在CentOS7系统安装步骤

Redis版本：7.0.15

服务器版本：CentOS Linux 7: x86_64

默认已经安装好了CentOS7系统，此文章只关注Redis的安装。

## 下载Redis

进入官网下载页面：[Redis](https://redis.io/downloads/)，在官网下面下载需要注意一下不要下载到了Redis Stack。

![image-20240412144726362](Redis安装.assets/image-20240412144726362.png)

![image-20240412145201727](Redis安装.assets/image-20240412145201727.png)

将下载好的Redis文件上传到你的机子上就可以了。

## 解压并安装



 **1.解压**
    ```sh
    tar -zvxf redis-7.0.15.tar.gz
    ```

解压后得到redis-7.0.15目录。我们将目录移动到/usr/local/redis-7.0.15目录下

```sh
mv /root/redis-7.0.15 /usr/local/redis-7.0.15
```

**2.编译**

- 进入redis目录中

  ```sh
  cd /usr/local/redis-7.0.15
  ```

- 执行编译

  ```sh
  make
  ```

- 安装

  ```sh
    make PREFIX=/usr/local/redis-7.0.15 install
  ```

    `PREFIX`：关键字的作用是编译的时候用于指定程序存放的路径。

    ![image-20240412154943380](Redis安装.assets/image-20240412154943380.png)

**3.启动**

​	在上面操作完成后，在/usr/local/redis-7.0.15目录下执行

```sh
./bin/redis-server& ./redis.conf
```

`&`：表示启动方式是采用后台进程方式

![image-20240412155550406](Redis安装.assets/image-20240412155550406.png)

测试：可以通过启动redis-cli程序来测试是否可以正常链接：

```sh
./bin/redis-cli
```

启动客户端，发送ping命令测试。

![image-20240412155857941](Redis安装.assets/image-20240412155857941.png)

## 查看redis-conf配置文件

**比较重要的配置项**

| 配制名称       | 配制项值范围                 | 说明                                                         |
| -------------- | ---------------------------- | ------------------------------------------------------------ |
| port           |                              | 指定 Redis 监听端口，默认端口为 6379                         |
| bind           |                              | 绑定的主机地址,如果需要设置远程访问则直接将这个属性备注下或者改为bind * 即可,这个属性和下面的protected-mode控制了是否可以远程访问 。 |
| daemonize      | yes/no                       | yes表示启用守护进程，默认是no即不以守护进程方式运行。其中Windows系统下不支持启用守护进程方式运行 |
| protected-mode | yes/no                       | 保护模式，该模式控制外部网是否可以连接redis服务，默认是yes,所以默认我们外网是无法访问的，如需外网连接rendis服务则需要将此属性改为no |
| timeout        |                              | 当客户端闲置多长时间后关闭连接，如果指定为0，表示关闭该功能(默认为0) |
| loglevel       | debug/verbose/notice/warning | 日志级别，默认为 notice                                      |
| databases      | 16                           | 设置数据库的数量，默认的数据库是0。整个通过客户端工具可以看得到 |
| rdbcompression | yes/no                       | 指定存储至本地数据库时是否压缩数据，默认为 yes，Redis 采用 LZF 压缩，如果为了节省 CPU 时间，可以关闭该选项，但会导致数据库文件变的巨大。 |
| dbfilename     | dump.rdb                     | 指定本地数据库文件名，默认值为 dump.rdb                      |
| dir            |                              | 指定本地数据库存放目录(默认 ./)                              |
| requirepass    |                              | 设置 Redis 连接密码，如果配置了连接密码，客户端在连接 Redis 时需要通过 AUTH <password> 命令提供密码，默认关闭 |
| maxclients     |                              | 设置同一时间最大客户端连接数，默认无限制，Redis 可以同时打开的客户端连接数为 Redis 进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis 会关闭新的连接并向客户端返回 max number of clients reached 错误信息。 |
| maxmemory      |                              | 指定 Redis 最大内存限制，Redis 在启动时会把数据加载到内存中，达到最大内存后，Redis 会先尝试清除已到期或即将到期的 Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。可以配合策略使用(maxmemory-policy)。 |
|                |                              |                                                              |

## 远程连接

修改redis-conf配置文件

```md
daemonize no
bind *	      	                        
protected-mode no
```

如果为了省事，可以将防火墙关闭：

```sh
systemctl status firewalld -- 查看防火墙的状态

systemctl stop firewalld  -- 关闭防火墙
```

开放端口：

```sh
firewall-cmd --zone=public --add-port=6379/tcp --permanent
#--zone=public 表示作用域为公共的
#--add-port=6379/tcp 添加tcp协议的端口6379
#--permanent 表示永久生效

#重新载入配置
firewall-cmd --reload

#查看开放端口
firewall-cmd --zone=public --list-ports
```



![image-20240412164240338](Redis安装.assets/image-20240412164240338.png)

![image-20240412164610224](Redis安装.assets/image-20240412164610224.png)

![image-20240412164633315](Redis安装.assets/image-20240412164633315-17129115938191.png)
