# Redis持久化

> Redis 的读写都是在内存中，所以它的性能较高，但在内存中的数据会随着服务器的重启而丢失，为了保证数据不丢失，我们需要将内存中的数据存储到磁盘，以便 Redis 重启时能够从磁盘中恢复原有的数据，而整个过程就叫做 Redis 持久化。主要分为两种方式，分别是RDB和AOF; 当然实际场景下还会使用这两种的混合模式。

可以参考：[Redis持久化官网文档](https://redis.io/docs/latest/operate/oss_and_stack/management/persistence/)

## <font color="#817F26">RDB持久化</font>

> **RDB**(Redis Database)：RDB persistence performs point-in-time snapshots of your dataset at specified intervals. 
>
> 在某个时间点，将数据生成快照保存到磁盘上，由于是某一时刻的快照，那么快照中的值要早于或者等于内存中的值。

### 触发机制

触发RDB持久化有`手动触发`和`自动触发`两种方式。

#### 1.手动触发

执行`save`或`bgsave`命令。

**save**：阻塞当前Redis服务器，直到RDB过程完成为止，对于内存 比较大的实例会造成长时间阻塞，线上环境不建议使用。

**bgsave**：Redis进程执行fork操作创建子进程，RDB持久化过程由子 进程负责，完成后自动结束。阻塞只发生在fork阶段，一般时间很短。（自动触发相当于执行bgsave命令）

#### 2.自动触发

自动触发RDB持久化条件

1. redis.conf中配置**save m n**，即在m秒内有n次修改时，自动触发bgsave生成rdb文件。
2. 主从复制时，从节点要从主节点进行全量复制时也会触发bgsave操作，生成当时的快照发送到从节点。
3. 执行**debug reload**命令重新加载redis时也会触发bgsave操作。
4. 默认情况下执行**shutdown**命令时，如果没有开启aof持久化，那么也会触发bgsave操作。

### RDB持久化流程

![save&bgsave](Redis持久化.assets/save&bgsave.svg)

​												**save & bgsave 流程**

![Redis-RDB持久化](Redis持久化.assets/bgsave-detail.svg)

​															**bgsave详细流程**



### RDB持久化配置

**周期性设置**

```conf
# 周期性执行条件的设置格式为
save <seconds> <changes>

# 默认的设置为：
# save 3600 1 300 100 60 10000

#   * After 3600 seconds (an hour) if at least 1 change was performed
#   * After 300 seconds (5 minutes) if at least 100 changes were performed
#   * After 60 seconds if at least 10000 changes were performed

# 以下设置方式为关闭RDB快照功能
save ""
```

可以通过`config get *` 命令查看所有配置信息。

```sh
config get save
1) "save"
2) "3600 1 300 100 60 10000"
```

**其他相关配置**

```conf
# 如果持久化出错，主进程是否停止写入
stop-writes-on-bgsave-error yes

# 是否压缩，使用LZF压缩
rdbcompression yes

# 导入时是否检查
rdbcompression yes

# 文件名称
dbfilename dump.rdb

# 文件保存路径
dir ./
```

### RDB优缺点

1. 优点（advantages）
   - RDB文件是某个时间节点的快照，默认使用LZF算法进行压缩，压缩后的文件体积远远小于内存大小，适用于备份、全量复制等场景。
   - Redis加载RDB文件恢复数据要远远快于AOF方式。
2. 缺点（disadvantages）
   - RDB方式实时性不够，无法做到秒级的持久化。
   - 每次调用bgsave都需要fork子进程，fork子进程属于重量级操作，频繁执行成本较高。
   - RDB文件是二进制的，没有可读性，AOF文件在了解其结构的情况下可以手动修改或者补全。
   - 版本兼容RDB文件问题。



## <font color="#817F26">AOF持久化</font>

> **AOF** (Append Only File): AOF persistence logs every write operation received by the server. These operations can then be replayed again at server startup, reconstructing the original dataset. Commands are logged using the same format as the Redis protocol itself.
>
> Redis是“写后”日志，Redis先执行命令，把数据写入内存，然后才记录日志。日志里记录的是Redis收到的每一条命令，这些命令是以文本形式保存。

### 触发机制



#### 1.手动触发

执行`bgrewriteaof`命令，立即触发aof重写。

#### 2.自动触发

根据配置规则来触发，当然自动触发的整体时间还跟Redis的定时任务频率有关系。

```conf
#表示当前aof文件空间和上一次重写后aof文件空间的比值，默认是aof文件翻倍触发重写
auto-aof-rewrite-percentage 100

#表示触发aof重写时aof文件的最小体积，默认64m
auto-aof-rewrite-min-size 64mb

```

**auto-aof-rewrite-percentage**的计算方法：

`auto-aof-rewrite-percentage` =（当前aof文件体积 - 上次重写后aof文件体积）/ 上次重写后aof文件体积 * 100%

**触发条件：**(`当前aof文件体积` > `auto-aof-rewrite-min-size`) && (`auto-aof-rewrite-percentage的计算值` > `配置文件中配置的auto-aof-rewrite-percentage值`)

### AOF重写流程

![bgrewriteaof](Redis持久化.assets/bgrewriteaof.svg)

### AOF持久化配置

Redis默认情况下，是没有开启AOF的，需要通过配置来开启AOF持久化。

```conf
#开启AOF持久化参数(yes为开启，no为关闭)
appendonly no

#aof持久化的文件名默认appendonly.aof
appendfilename "appendonly.aof"

#文件保存路径名
appenddirname "appendonlydir"


#同步策略， 默认everysec
# appendfsync always
appendfsync everysec
# appendfsync no

#aof重写期间是否同步
no-appendfsync-on-rewrite no

#触发重写配置
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb

#加载aof出错如何处理（yes会启动服务，把错误日志发给用户，no则不会启动服务）
aof-load-truncated yes


#文件重写策略
aof-rewrite-incremental-fsync
```

同步策略

| 配置项   | 写回时机         | 优点                       | 缺点                             |
| -------- | ---------------- | -------------------------- | -------------------------------- |
| always   | 同步写回         | 可靠性高，数据基本不会丢失 | 每个写命令都要落盘，想能影响较大 |
| everysec | 每秒写回         | 性能适中                   | 宕机时会丢失1s数据               |
| no       | 操作系统控制写回 | 性能好                     | 宕机时会丢失比较多的数据         |



### AOF优缺点

1. 优点
   - 数据安全性能较高，如果配置everysec策略，最多丢失1秒数据。
   - aof文件比rdb文件可读性高，便于容灾。
2. 缺点
   - aof文件体积比rdb文件体积大，不利于传输。
   - aof恢复速度要比rdb恢复速度慢。



## 重启加载

![redis-restart](Redis持久化.assets/redis-restart.svg)

1. redis重启时判断是否开启aof，如果开启了aof，那么就优先加载aof文件；
2. 如果aof存在，那么就去加载aof文件，加载成功的话redis重启成功，如果aof文件加载失败，那么会打印日志表示启动失败，此时可以去修复aof文件后重新启动；
3. 若aof文件不存在，那么redis就会转而去加载rdb文件，如果rdb文件不存在，redis直接启动成功；
4. 如果rdb文件存在就会去加载rdb文件恢复数据，如加载失败则打印日志提示启动失败，如加载成功，那么redis重启成功，且使用rdb文件恢复数据。
