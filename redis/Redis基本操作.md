

# Redis基本操作



## Redis基本数据结构

1. **字符串(String)**：简单的键值对。
2. **哈希(Hash)**：类似于关联数组，适合存储对象。
3. **列表(List)**：有序的字符串列表。
4. **集合(Set)**：不重复的字符串集合。
5. **有序集合(Sorted Set)**：类似于集合，但每个元素都有一个分数，可以按照分数排序。

通过Java代码有两种方式连接Redis(Jedis、Lettuce)下面的Java示例使用Jedis来实现。[官方文档](https://redis.io/docs/latest/develop/connect/clients/java/jedis/)。

引入pom文件

```xml
<dependency>
    <groupId>redis.clients</groupId>
    <artifactId>jedis</artifactId>
    <version>5.1.2</version>
</dependency>
```



### String类型

通过连接池来连接

```java
    public static void main(String[] args) {

        JedisPool pool = new JedisPool("192.168.0.129",6379);

        try(Jedis jedis = pool.getResource()) {
            jedis.set("greet","hello");
            System.out.println(jedis.get("greet"));
        }
    }
```

String类型时最简单的数据类型之一，可以存储的值包括字符串、整数或者浮点数。

1. **设置值**

   ```sh
   set key value
   ```

   redis-cli

   ```sh
   set greet hello
   
   # OK
   ```

   jedis

   ```java
   String status = jedis.set("greet", "hello");
   System.out.println(status); //OK
   ```

   

2. **获取值**

   ```sh
   get key
   ```

   redis-cli

   ```sh
   get greet 
   
   # "hello"
   ```

   jedis

   ```java
   String value = jedis.get("greet");
   System.out.println(value) //hello
   ```

3. **设置带有过期时间的值**

   ```sh
   setex key seconds value
   ```

   redis-cli

   ```sh
   setex name 10 zhangsan
   # OK
   # 执行get name zhangsan
   # 过10s后再次执行 get name nil
   ```

   jedis

   ```java
   String setexStatus = jedis.setex("name", 10, "lisi");
   System.out.println(setexStatus); //ok
   ```

4. **追加值**

   ```sh
   append key value
   ```

   redis-cli

   ```sh
   append greet update
   
   #11 
   #get greet helloupdate
   ```

   jedis

   ```java
   long append = jedis.append("greet", "world");
   System.out.println(append); //16 helloupdateworld
   ```

5. **获取值的子字符串**

   ```sh
   getrange key start end
   ```

   redis-cli

   ```sh
   # get greet  helloupdateworld
   getrange greet 5 10
   
   #update(下标从0开始，左闭右闭)
   ```

   jedis

   ```java
   String greet = jedis.getrange("greet", 0, 4);
   System.out.println(greet); //hello
   ```

6. **设置指定偏移量的值**

   ```sh
   setrange key offset value
   ```

   redis-cli

   ```sh
   setrange greet 5 _world
   
   #16(从下标开始，开始设置)
   # get greet "hello_worldworld"
   
   #如果offset下标值大于字符串长度,会补齐空字符
   setrange greet 20 !!!
   #"hello_worldworld\x00\x00\x00\x00!!!"
   #hello_worldworld    !!!
   
   ```

   jedis

   ```java
   long setrange = jedis.setrange("name", 3, "_hello_");
   System.out.println(jedis.get("name")); //   _hello_
   System.out.println(setrange);//10
   
   //如果key 不存在，一样是前面补齐offset个空字符
   ```

7. **获取值的长度**

   ```sh
   strlen key
   ```

   redis-cli

   ```sh
   strlen greet
   
   # 23 
   ```

   jedis

   ```java
   long greet = jedis.strlen("greet");
   System.out.println(greet); //23
   ```

8. **递增**

   ```sh
   incr key
   
   #如果不是整数则会报错
   #ERR value is not an integer or out of range
   ```

   redis-cli

   ```sh
   set age 12
   incr age
   
   # 13 返回递增后的值
   ```

   jedis

   ```java
   System.out.println(jedis.get("age")); //13
   long age = jedis.incr("age");
   System.out.println(age); //14
   ```

9. **递减**

   ```sh
   decr key
   
   #如果不是整数则会报错
   #ERR value is not an integer or out of range
   ```

   redis-cli

   ```sh
   decr age
   
   # 13 返回递增减的值
   ```

   jedis

   ```java
   System.out.println(jedis.get("age")); //13
   long age = jedis.decr("age");
   System.out.println(age); //12
   ```

10. **增加指定的值**

    ```sh
    incrby key increment
    
    #如果不是整数则会报错
    #ERR value is not an integer or out of range
    ```

    redis-cli

    ```sh
    incrby age 10
    
    #22
    ```

    jedis

    ```java
    System.out.println(jedis.get("age")); //22
    long age = jedis.incrBy("age",10);
    System.out.println(age); //32
    ```

11. 减少指定的值

    ```sh
    decrby key decrement
    
    #如果不是整数则会报错
    #ERR value is not an integer or out of range
    ```

    redis-cli

    ```sh
    decrby age 10
    
    #22
    ```

    jedis

    ```java
    System.out.println(jedis.get("age")); //22
    long age = jedis.decrBy("age",10);
    System.out.println(age); //12
    ```



### Hash类型

Hash类型是用来存储对象的一种数据结构，类似关联数组或者字典，可以存储第多个字段与值的映射关系。

1. **设置字段与值的映射关系**

   ```sh
   hset key field value
   ```

   redis-cli

   ```sh
   hset user:id:111 username zhangsan
   
   # 1返回添加数
   hset user:id:111 age 12
   
   # 1返回添加数
   ```

   jedis

   ```java
   long hset = jedis.hset("user:id:112", "username", "lisi");
   System.out.println(hset); //1
   long hset1 = jedis.hset("user:id:112", "age", "13");
   System.out.println(hset1); //1
   ```

2. **获取字段对应值**

   ```sh
   hget key field
   ```

   redis-cli

   ```sh
   hget user:id:111 username
   #"zhangsan"
   hget user:id:111 age
   #"12"
   ```

   jedis

   ```java
   String username = jedis.hget("user:id:112", "username");
   String age = jedis.hget("user:id:112", "age");
   String name = jedis.hget("user:id:112", "name");
   System.out.println(username); //lisi
   System.out.println(age); //13
   System.out.println(name); //null
   
   //不存在的field会返回null
   ```

3. **获取所有字段与值的映射关系**

   ```sh
   hgetall key
   ```

   redic-cli

   ```sh
   hgetall user:id:111
   
   #1) "username"
   #2) "zhangsan"
   #3) "age"
   #4) "12"
   ```

   jedis

   ```java
   Map<String, String> user = jedis.hgetAll("user:id:112");
   user.forEach((k,v)->{
       System.out.println("field:" + k + "\tvalue:" + v);
   });
   
   //field:age	value:13
   //field:username	value:lisi
   ```

4. **获取所有字段名**

   ```sh
   hkeys key
   ```

   redis-cli

   ```sh
   hkeys user:id:111
   #1) "username"
   #2) "age"
   ```

   jedis

   ```java
   Set<String> userFields = jedis.hkeys("user:id:112");
   userFields.forEach(System.out::println);
   //age
   //username
   ```

5. **获取所有字段值**

   ```sh
   hvals key
   ```

   redis-cli

   ```sh
   hvals user:id:111
   #1) "zhangsan"
   #2) "12"
   ```

   jedis

   ```java
   List<String> userFieldValues = jedis.hvals("user:id:112");
   userFieldValues.forEach(System.out::println);
   
   //lisi
   //13
   ```

6. **检查字段是否存在**

   ```sh
   hexists key field
   
   # 存在返回1不存在返回0
   ```

   redis-cli

   ```sh
   hkeys user:id:111
   #1) "username"
   #2) "age"
   hexists user:id:111 username
   #1
   hexists user:id:111 name
   #0
   ```

   jedis

   ```java
   boolean username = jedis.hexists("user:id:112", "username");
   System.out.println(username); //true
   boolean name = jedis.hexists("user:id:112", "name");
   System.out.println(name); //false
   ```

7. **删除指定字段**

   ```sh
   hdel key field [field...]
   ```

   redis-cli

   ```sh
   hdel user:id:111 username
   #1
   hdel user:id:111 username
   #0
   #删除存在的field返回1不存在的返回0
   ```

   jedis

   ```java
   long hdel = jedis.hdel("user:id:112", "username", "name");
   System.out.println(hdel); //1
   
   //只要有存在的就会返回1.换句话说，就是删除过字段就会返回1
   ```

8. **获取字段数量**

   ```sh
   hlen key
   ```

   redis-cli

   ```sh
   hlen user:id:111
   #1
   ```

   jedis

   ```java
   long hlen = jedis.hlen("user:id:112");
   System.out.println(hlen); //1
   ```

9. **增加字段整数值**

   ```sh
   hincrby key field increment
   ```

   redis-cli

   ```sh
   hincrby user:id:111 age 10
   #22
   #返回增加后的值
   ```

   jedis

   ```java
   long age = jedis.hincrBy("user:id:112", "age", 20);
   System.out.println(age); //33
   ```

10. **增加浮点数字段值**

   ```sh
   hincrbyfloat key field increment
   ```

   redis-cli

   ```sh
   hset user:id:111 salary 3509.89
   hget user:id:111 salary
   #"3509.89"
   hincrbyfloat user:id:111 salary 100
   #"3609.8899999999999999"
   #返回增加后的值
   ```

   jedis

   ```java
   double salary = jedis.hincrByFloat("user:id:111", "salary", -100);
   System.out.println(salary); //3509.89
   ```

###  List类型

List类型是一个有序的字符串列表，可以在列表的两端进行插入、删除操作。

1. **左侧/右侧插入一个或多个值**

   ```sh
   #左插
   lpush key value [value...]
   #右插
   rpush key value [value...]
   #返回列表长度
   ```

   redis-cli

   ```sh
   lpush score 80
   #1
   lpush score 100 70
   #3
   rpush score 50 60
   #5
   
   ```

   jedis

   ```java
   long lpush = jedis.lpush("mylist", "hello", "world");
   System.out.println(lpush); //2
   long rpush = jedis.rpush("mylist", "redis", "hello", "!!!");
   System.out.println(rpush); //5
   ```

2. **获取指定范围值**

   ```sh
   lrange key start stop
   
   #如果start=0 stop=-1则代表返回所有元素
   ```

   redis-cli

   ```sh
   lrange score 0 -1
   #1) "70"
   #2) "100"
   #3) "80"
   #4) "50"
   #5) "60"
   ```

   jedis

   ```java
   List<String> mylist = jedis.lrange("mylist", 1, 4);
   mylist.forEach(System.out::println);
   //hello
   //redis
   //hello
   //!!!
   ```

3. **获取指定位置元素**

   ```sh
   lindex key index
   
   #如果index是正数就是从左往右的位置，index为负数则是从右往左
   #不管是正数还是负数，只要index绝对值超过列表长度减一就会返回空
   ```

   redis-cli

   ```sh
   lindex score 0
   #70
   lindex score 8
   #nil
   lindex score -1
   #50
   lindex score -6
   #nil
   ```

   jedis

   ```java
   String mylist = jedis.lindex("mylist", 4);
   System.out.println(mylist); //!!!
   ```

4. **获取列表长度**

   ```sh
   llen key
   ```

   redis-cli

   ```sh
   llen score
   #5
   ```

   jedis

   ```java
   long mylist = jedis.llen("mylist");
   System.out.println(mylist); //5
   ```

5. **移除并返回左侧/右侧元素**

   ```sh
   #左侧
   lpop key
   #右侧
   rpop key
   ```

   redis-cli

   ```sh
   lpop score
   #"70"
   rpop score
   #"60"
   ```

   jedis

   ```java
   String lvalue = jedis.lpop("mylist");
   String rvalue = jedis.rpop("mylist");
   System.out.println(lvalue); //world
   System.out.println(rvalue); //!!!
   long mylist = jedis.llen("mylist");
   System.out.println(mylist); //3
   ```

6. **设置指定位置的元素**

   ```sh
   lset key index value
   
   #超出列表返回会报错 ERR index out of range
   #不存在列表会报 ERR no such key
   ```

   redis-cli

   ```sh
   lset score -1 20
   #OK
   ```

   jedis

   ```java
   String lset = jedis.lset("mylist", 0, "who");
   System.out.println(lset); //OK 
   ```

7. **在指定元素之前/之后插入元素**

   ```sh
   #之前
   linsert key before pivot value
   #之后
   linsert key after pivot value
   
   #没有找到指定元素 pivot 则返回-1.否则返回列表长度
   #有多个相同的pivot， 则会在第一个pivot的位置操作
   ```

   redis-cli

   ```sh
   linsert score before 0 80
   #-1
   linsert score before 80 80
   #4
   linsert score after 80 100
   #5
   ```

   jedis

   ```java
   long linsert1 = jedis.linsert("mylist", ListPosition.BEFORE, "00", "hello");
   long linsert2 = jedis.linsert("mylist", ListPosition.AFTER, "who", "hello");
   System.out.println(linsert1); //-1
   System.out.println(linsert2); //4
   ```

8. **移除列表中指定值**

   ```sh
   lrem key count value
   ```

   redis-cli

   ```sh
   lrem score 1 80
   #1 
   lrem score 1 6
   #0 不存在
   lrem score 3 100
   #2 移除掉了2个匹配的值
   lpush score 80 80 80
   lrem score 0 80
   #3 如果Count为0 则表示移除全部匹配的值
   ```

   jedis

   ```java
   long lrem = jedis.lrem("mylist", 0, "hello");
   System.out.println(lrem); //2
   ```

 ### Set类型

Set类型是一个无序的、不重复的字符串集合、支持添加、删除、查询操作。

1. **添加一个或多个元素**

   ```sh
   sadd key member [member...]
   
   #返回添加个数
   ```

   redis-cli

   ```sh
   sadd nums 1
   #1
   sadd nums 1 2 3 4 5
   #5
   ```

   jedis

   ```java
   long sadd = jedis.sadd("phoneBrand", "Apple", "华为", "vivo");
   System.out.println(sadd); //3
   ```

2. **获取所有成员**

   ```sh
   smembers key
   
   #返回集合所有元素，如果key不存则返回空集合
   ```

   redis-cli

   ```sh
   smembers nums
   
   # 3 1 5 4 2
   ```

   jedis

   ```java
   Set<String> phoneBrand = jedis.smembers("phoneBrand");
   phoneBrand.forEach(System.out::println);
   //Apple
   //华为
   //vivo
   ```

3. **检查集合中是否包含指定成员**

   ```sh
   sismember key member
   
   #存在返回1，不存在返回0
   ```

   redis-cli

   ```sh
   sismember nums 2
   #1
   sismember nums 10
   #0
   
   ```

   jedis

   ```java
   boolean sismember = jedis.sismember("phoneBrand", "Apple");
   System.out.println(sismember); //true
   boolean sismember1 = jedis.sismember("phoneBrand1", "Apple");
   System.out.println(sismember1); //false
   ```

4. **获取集合成员数量**

   ```sh
   scard key
   ```

   redis-cli

   ```sh
   scard nums
   #6 存在返回集合数量
   scard ds
   #0 不存在集合返回0
   
   ```

   jedis

   ```java
   long phoneBrand = jedis.scard("phoneBrand");
   long phoneBrand1 = jedis.scard("phoneBrand1");
   System.out.println(phoneBrand); //3
   System.out.println(phoneBrand1); //0
   ```

5. **从集合移除指定成员**

   ```sh
   srem key member [member...]
   
   #返回移除数量
   ```

   redis-cli

   ```sh
   srem nums 1 23
   #1
   ```

   jedis

   ```java
   long srem = jedis.srem("phoneBrand", "Apple");
   System.out.println(srem); //1
   long srem1 = jedis.srem("phoneBrand1", "Apple");
   System.out.println(srem1); //0
   ```

6. **从集合中随机移除并返回一个成员**

   ```sh
   spop key [count]
   #返回移除的成员
   ```

   redis-cli

   ```sh
   sadd nums 9 8 7 6 5
   # 4
   spop nums
   #"6"
   spop nums 2
   #1) "1\\"
   #2) "8"
   ```

   jedis

   ```java
   String phoneBrand = jedis.spop("phoneBrand");
   System.out.println(phoneBrand); //vivo
   ```

7. 获取多个集合的交集/并集/差集

   ```sh
   #交集
   sinter key [key...]
   #并集
   sunion key [key...]
   #差集
   sdiff key [key...]
   
   #是第一个key与后面的key做操作
   ```

   redis-cli

   ```sh
   #nums 9 7 5
   sadd nums1 5 8 3
   #3
   sinter nums nums1
   #"5"
   sunion nums nums1
   #1) "3"
   #2) "5"
   #3) "7"
   #4) "8"
   #5) "9"
   sdiff nums nums1
   #1) "7"
   #2) "9"
   sdiff nums1 nums
   #1) "3"
   #2) "8"
   sinter nums
   #1) "5"
   #2) "9"
   #3) "7"
   
   ```

   jedis

   ```java
   //phoneBrand ["Apple", "华为", "vivo"]
   long sadd = jedis.sadd("phoneBrand1", "apple", "华为", "vivo","Samsung");
   Set<String> sinter = jedis.sinter("phoneBrand1", "phoneBrand");
   Set<String> sunion = jedis.sunion("phoneBrand1", "phoneBrand");
   Set<String> sdiff = jedis.sdiff("phoneBrand1", "phoneBrand");
   
   System.out.println("sinter:"+ sinter.toString());//sinter:[华为, vivo]
   System.out.println("sunion:"+ sunion.toString());//sunion:[apple, Apple, 华为, Samsung, vivo]
   System.out.println("sdiff:"+ sdiff.toString());//sdiff:[apple, Samsung]
   ```

8. **将指定成员从一个集合移动到另一集合**

   ```sh
   smove source destination member
   
   #移动成功返回1失败返回0
   ```

   redis-cli

   ```sh
   smove nums1 nums 8
   #1
   smove nums1 nums 0
   #0
   
   ```

   jedis

   ```java
   long smove = jedis.smove("phoneBrand1", "phoneBrand", "vivo");
   System.out.println(smove); //1
   
   long smove1 = jedis.smove("phoneBrand1", "phoneBrand", "apple");
   System.out.println(smove1);//1
   
   //就算目标集合有待移动的成员，也会从源集合移走
   ```

### SortedSet类型

Sorted Set类型是一个有序的集合，每个成员都关联一个分数，根据分数对成员进行排序。Sorted Set类型常用于需要排序的场景，例如排行榜、计数器等。

1. **添加成员，并指定分数**

   ```sh
   zadd key score member [score member...]
   ```

   redis-cli

   ```sh
   zadd leaderboard 100 player1
   # 1
   zadd leaderboard 200 player2 300 player3
   # 2
   #返回添加的个数
   ```

   jedis

   ```java
   long zadd = jedis.zadd("leaderboard", 400, "player4");
   System.out.println(zadd); //1
   ```

2. **获取集合中指定范围的成员**

   ```sh
   zrange key start stop [withscores]
   ```

   redis-cli

   ```sh
   zrange leaderboard 0 -1 withscores
   #1) "player1"
   #2) "100"
   #3) "player2"
   #4) "200"
   #5) "player3"
   #6) "300"
   #7) "player4"
   #8) "400"
   #withscores 表示显示成员分数
   ```

   jedis

   ```java
   List<String> leaderboard = jedis.zrange("leaderboard", 0, -1);
   leaderboard.forEach(System.out::println);
   System.out.println("////////////////");
   List<String> leaderboard1 = jedis.zrange("leaderboard", ZRangeParams.zrangeByScoreParams(200, 400));//通过分数范围
   leaderboard1.forEach(System.out::println);
   
   //player1
   //player2
   //player3
   //player4
   ////////////////
   //player2
   //player3
   //player4
   ```

3. **获取集合中指定分数范围的成员**

   ```sh
   zrangebyscore key min max [withscores] [limit offset count]
   ```

   redis-cli

   ```sh
   zrangebyscore leaderboard 100 200 withscores limit 0 10
   #1) "player1"
   #2) "100"
   #3) "player2"
   #4) "200"
   zrangebyscore leaderboard 100 200 withscores limit 0 1
   #1) "player1"
   #2) "100"
   
   ```

   jedis

   ```java
    List<String> leaderboard = jedis.zrangeByScore("leaderboard", 100, 500, 0, 2);
   leaderboard.forEach(System.out::println);
   //player1
   //player2
   ```

4. **获取集合中成员的分数**

   ```sh
   zscore key member
   ```

   redis-cli

   ```sh
   zscore leaderboard player1
   #"100"
   ```

   jedis

   ```java
   Double zscore = jedis.zscore("leaderboard", "player4");
   System.out.println(zscore); //400.0
   Double zscore1 = jedis.zscore("leaderboard", "player6");
   System.out.println(zscore1);//null
   ```

5. **获取集合成员的排名**

   ```sh
   #从小到大
   zrank key member
   #从大到小
   zrevrank key member
   ```

   redis-cli

   ```sh
   zrank leaderboard player1
   # 0
   zrank leaderboard player4
   # 3
   zrevrank leaderboard player1
   # 3
   ```

   jedis

   ```java
   Long zrank = jedis.zrank("leaderboard", "player3");
   System.out.println(zrank); //2
   Long zrank1 = jedis.zrank("leaderboard", "player0");
   System.out.println(zrank1); //null
   Long zrevrank = jedis.zrevrank("leaderboard", "player3");
   System.out.println(zrevrank); //1
   ```

6. **获取集合成员数量**

   ```sh
   zcard key
   ```

   redis-cli

   ```sh
   zcard leaderboard
   # 4
   
   ```

   jedis

   ```java
   long leaderboard = jedis.zcard("leaderboard");
   System.out.println(leaderboard);//4
   ```

7. **增加指定成员分数**

   ```sh
   zincrby key increment member
   #返回增加后的分数
   #如果member不存在，则会新添加这个成员
   ```

   redis-cli

   ```sh
   zincrby leaderboard 50 player1
   #"150" 
   zincrby leaderboard 50 player0
   #"50"
   ```

   jedis

   ```java
   double zincrby = jedis.zincrby("leaderboard", 45, "player2");
   System.out.println(zincrby); //245.0
   ```

8. **移除指定成员**

   ```sh
   zrem key member [member...]
   #返回移除成功个数
   ```

   redis-cli

   ```sh
   zrem leaderboard player1
   # 1
   zrem leaderboard player0
   # 1
   zrem leaderboard player10
   # 0
   ```

   jedis

   ```java
   long zrem = jedis.zrem("leaderboard", "player2","player3");
   System.out.println(zrem); //2
   ```

9. **移除指定分数范围成员**

   ```sh
   zremrangebyscore key min max
   ```

   redis-cli

   ```sh
   zremrangebyscore leaderboard 0 100
   # 0
   zremrangebyscore leaderboard 0 1000
   # 1
   
   ```

   jedis

   ```java
   long leaderboard = jedis.zremrangeByScore("leaderboard", 0, 100);
   System.out.println(leaderboard); //0
   ```

   

